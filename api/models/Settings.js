/**
 * Settings.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    surveyId: {
      model: 'Survey',
      required: true
    },

    chartsSettings: {
      type: 'json'
    },

    operatorPdfLayout: {
      type: 'json'
    },

    tasterPdfLayout: {
      type: 'json'
    }
  }
}
