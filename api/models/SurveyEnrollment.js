/**
 * SurveyEnrollment.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    user: {
      model: 'User'
    },

    survey: {
      model: 'Survey',
      required: true
    },

    state: {
      type: 'string',
      isIn: [
        'waiting-for-screening',
        'waiting-for-product',
        'finished',
        'paid',
        'rejected'
      ],
      defaultsTo: 'waiting-for-screening'
    },

    selectedProducts: {
      collection: 'Product',
      via: 'surveyEnrollments'
    },

    answers: {
      collection: 'Answer',
      via: 'enrollment'
    },

    lastAnsweredQuestion: {
      model: 'Question'
    },

    paypalEmail: {
      type: 'string',
      defaultsTo: ''
    },

    finishedAt: {
      type: 'string'
    },

    referral: {
      type: 'string'
    },

    savedRewards: {
      type: 'JSON',
      defaultsTo: []
    },

    processed: {
      type: 'boolean',
      defaultsTo: false
    },

    validation: {
      type: 'string',
      required: false
    },

    comment: {
      type: 'string',
      required: false
    },

    shareExported: {
      type: 'boolean',
      required: false
    },

    productIncentiveExported: {
      type: 'boolean',
      required: false
    },

    giftCardIncentiveExported: {
      type: 'boolean',
      required: false,
      defaultsTo: false
    },

    browserInfo: {
      type: 'JSON',
      defaultsTo: []
    },

    lastSelectedProduct: {
      type: 'string',
      defaultsTo: ''
    },

    productDisplay: {
      model: 'ProductDisplay'
    },

    productDisplayOrder: {
      type: 'json',
      columnType: 'array',
      defaultsTo: []
    },

    productDisplayType: {
      type: 'string',
      defaultsTo: 'none'
    }
  }
}
