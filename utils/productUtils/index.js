module.exports = {
  createProduct: require('./createProduct'),
  updateProduct: require('./updateProduct')
}
