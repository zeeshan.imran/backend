/* eslint-disable no-console */
console.log('--- Flavorwiki Backend Server ---')
console.log(`  - Server mode : ${process.env.NODE_ENV}`)
console.log(`  - APP_URL     : ${process.env.APP_URL}`)
console.log(`  - STATS_API   : ${process.env.STATS_API}`)
console.log()
