/* globals QrCode */
// eslint-disable-next-line node/no-extraneous-require
const ObjectId = require('mongodb').ObjectID
const createSearchService = require('../../../utils/createSearchService')
const queryBuilder = require('../../../utils/mongoQueryBuilder')
const qrCodeSearch = createSearchService(QrCode)

const createQrCodeSearchPipeline = ({ owner, keyword }) => {
  let $match = {}

  if (owner) {
    $match = {
      ...$match,
      owner: ObjectId(owner)
    }
  }

  if (keyword) {
    $match = {
      ...$match,
      name: { $regex: queryBuilder.toRegExp(`%${keyword}%`) }
    }
  }

  return [{ $match }]
}

module.exports = {
  qrCodeSearch,
  createQrCodeSearchPipeline
}
