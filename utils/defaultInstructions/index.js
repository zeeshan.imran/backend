const defaultInstructions = [
  'Please be sure you have all of your selected products before starting the tasting.',
  'Find a quiet place.',
  'You will be given additional instructions during the tasting.',
  'When you are ready, click ‘Start Tasting’ below.'
]

module.exports = defaultInstructions
