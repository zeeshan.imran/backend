/**
 * QrCode.js
 *
 * @description :: QrCode
 */

module.exports = {
  attributes: {
    owner: {
      model: 'Organization',
      required: true
    },

    name: {
      type: 'string',
      required: true
    },

    uniqueName: {
      type: 'string',
      required: true
    },

    targetType: {
      type: 'string',
      required: true,
      isIn: ['link', 'survey']
    },

    survey: {
      type: 'string',
      required: false,
      allowNull: true
    },

    targetLink: {
      type: 'string',
      required: false,
      allowNull: true
    },

    qrCodePhoto: {
      type: 'string',
      required: true
    },

    creator: {
      model: 'User'
    },

    modifier: {
      model: 'User'
    }
  }
}
