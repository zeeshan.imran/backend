/* globals Answer */
const R = require('ramda')
// eslint-disable-next-line node/no-extraneous-require
const ObjectId = require('mongodb').ObjectID

const answerSearch = {
  funnel: async function (questions) {
    const questionIds = R.map(question => {
      return ObjectId(question.id)
    }, questions)

    const aggregateArray = [{ $match: { question: { $in: questionIds } } }]
    aggregateArray.push({
      $group: {
        _id: { product: '$product', question: '$question' },
        responses: { $sum: 1 }
      }
    })
    aggregateArray.push({
      $lookup: {
        from: 'product',
        localField: '_id.product',
        foreignField: '_id',
        as: 'product'
      }
    })

    aggregateArray.push({
      $unwind: { path: '$product', preserveNullAndEmptyArrays: true }
    })

    aggregateArray.push({
      $project: {
        _id: 0,
        question: '$_id.question',
        product: { id: '$product._id', name: 1, responses: '$responses' },
        responses: 1
      }
    })

    aggregateArray.push({
      $group: {
        _id: '$question',
        product: { $addToSet: '$product' },
        responses: { $sum: '$responses' }
      }
    })

    aggregateArray.push({
      $project: {
        _id: 0,
        question: '$_id',
        product: 1,
        responses: 1
      }
    })

    const db = Answer.getDatastore().manager
    const cursor = await db
      .collection(Answer.tableName)
      .aggregate(aggregateArray)

    const docs = await cursor.toArray()

    return docs
  }
}

module.exports = {
  answerSearch
}
