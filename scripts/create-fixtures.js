/* globals Organization, QrCode, Product, Survey, Question, PairQuestion, User, SurveyEnrollment, Answer, Brand, sails */
const cerealBarSurveySeed = require('./cereal-bar-survey')
const icedTeaSurveySeed = require('./iced-tea-survey')
const { createSet } = require('./generators')

const clearDatabase = async () => {
  await Answer.destroy({})
  await SurveyEnrollment.destroy({})
  await Product.destroy({})
  await Brand.destroy({})
  await PairQuestion.destroy({})
  await Question.destroy({})
  await Survey.destroy({})
  await User.destroy({})
  await Organization.destroy({})
  await QrCode.destroy({})
}

const organizations = [
  {
    name: 'Flavorwiki',
    uniqueName: 'flavorwiki'
  },
  {
    name: 'Other Organization',
    uniqueName: 'other-organization'
  }
]

const users = [
  {
    emailAddress: 'operator@flavorwiki.com',
    type: 'operator',
    password: 'operator',
    isSuperAdmin: true,
    organizationId: 0,
    language: 'german'
  },
  {
    emailAddress: 'other.operator@flavorwiki.com',
    type: 'operator',
    password: 'operator',
    organizationId: 0,
    isTaster: true,
    language: 'english'
  },
  {
    emailAddress: 'jon.snow@flavorwiki.com',
    isSuperAdmin: true,
    type: 'operator',
    password: 'operator',
    language: 'german'
  },
  {
    emailAddress: 'aegon.targaryen@flavorwiki.com',
    isSuperAdmin: true,
    type: 'operator',
    password: 'operator',
    language: 'english'
  },
  {
    emailAddress: 'jaqen.hgar@flavorwiki.com',
    type: 'power-user',
    password: 'operator',
    language: 'german'
  },
  {
    emailAddress: 'daemon.blackfyre@flavorwiki.com',
    type: 'power-user',
    password: 'operator',
    language: 'english'
  },
  {
    emailAddress: 'kmtank.qa@gmail.com',
    type: 'operator',
    password: 'operator',
    organizationId: 0,
    language: 'german'
  },
  {
    emailAddress: 'taster@flavorwiki.com',
    type: 'taster',
    password: 'operator',
    language: 'english'
  }
]

const qrCodes = [
  {
    name: 'Cereal bar survey',
    uniqueName: 'cereal-bar-survey',
    targetType: 'survey',
    survey: '**will be assign late**',
    qrCodePhoto:
      'https://s3-eu-west-1.amazonaws.com/flavorwiki-storage/qr-code/flavorwiki/cereal-bar-survey.png'
  },
  {
    name: 'Flavorwiki slack channel',
    uniqueName: 'flavor-wiki-slack',
    targetType: 'link',
    targetLink: 'https://flavor-wiki.slack.com',
    qrCodePhoto:
      'https://s3-eu-west-1.amazonaws.com/flavorwiki-storage/qr-code/flavorwiki/flavor-wiki-slack.png'
  }
]

module.exports = {
  friendlyName: 'Create fixtures',
  description: 'Generating Project Fixtures',

  fn: async function (inputs, exits) {
    sails.log('Running custom shell script... (`create-fixtures`)')

    sails.log('Cleaning previous database...')
    await clearDatabase()

    sails.log('Creating organizations...')
    const createdOrganizations = await createSet(Organization, organizations)

    sails.log('Creating Users...')
    const usersWithOrganizations = users.map((user, userIndex) =>
      user.type === 'operator'
        ? Object.assign({}, user, {
          organization:
              typeof user.organizationId === 'number'
                ? createdOrganizations[user.organizationId].id
                : null
        })
        : user
    )
    const createdUsers = await createSet(User, usersWithOrganizations)

    const operatorUsersIds = createdUsers
      .filter(user => user.type === 'operator')
      .map(user => user.id)
    organizations.map((org, orgIndex) =>
      Object.assign({}, org, { members: operatorUsersIds[orgIndex] })
    )

    sails.log('Base fixtures added!')

    // All done.
    const operatorId = operatorUsersIds[0]
    const ownerId = createdOrganizations[0].id
    const creatorId = createdUsers[0].id

    await icedTeaSurveySeed({ owner: ownerId, creator: creatorId })

    const cerealBarSurvey = await cerealBarSurveySeed({ owner: ownerId })

    //
    qrCodes[0].survey = cerealBarSurvey.uniqueName
    await createSet(
      QrCode,
      qrCodes.map(qrCode => ({
        ...qrCode,
        owner: ownerId,
        creator: operatorId
      }))
    )

    return exits.success()
  }
}
