module.exports = {
  updateQuestion: require('./updateQuestion'),
  createQuestion: require('./createQuestion'),
  cleanPairs: require('./cleanPairs'),
  generatePairs: require('./generatePairs')
}
