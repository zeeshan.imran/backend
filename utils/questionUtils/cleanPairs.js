/* globals PairQuestion */

module.exports = async questionId => {
  await PairQuestion.destroy({ question: questionId })
}
