const R = require('ramda')

const getUnanswerdPairs = (allPairs, answeredPairsIds) => {
  const allPairsById = allPairs.reduce((byId, pair) => {
    return Object.assign({}, byId, { [pair.id]: pair })
  }, {})

  const mixedPairs = R.concat(allPairs.map(({ id }) => id), answeredPairsIds)

  const notAnsweredPairsIds = R.uniq(mixedPairs)
  const notAnsweredPairs = notAnsweredPairsIds.map(id => allPairsById[id])

  return notAnsweredPairs
}

module.exports = getUnanswerdPairs
