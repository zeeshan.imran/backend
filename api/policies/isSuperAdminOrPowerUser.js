/**
 * isSuperAdminOrPowerUser
 *
 * A simple policy that blocks requests from non-super-admins.
 *
 * For more about how to use policies, see:
 *   https://sailsjs.com/config/policies
 *   https://sailsjs.com/docs/concepts/policies
 *   https://sailsjs.com/docs/concepts/policies/access-control-and-permissions
 */
module.exports = function (req, res, proceed) {
  if (!req.token) {
    return res.unauthorized()
  }

  const { user, superUser } = req.token

  if (superUser && (superUser.isSuperAdmin || superUser.type === 'power-user')) {
    return proceed()
  }

  if (!user.isSuperAdmin && user.type !== 'power-user') {
    return res.forbidden()
  }

  return proceed()
}
