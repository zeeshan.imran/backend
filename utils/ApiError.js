module.exports = class ApiError {
  constructor (code, error) {
    this.code = code
    this.error = error
  }

  toJSON () {
    return {
      code: this.code,
      error: this.error
    }
  }
}
