/* globals ProductDisplay */
/**
 * CreateCombination
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const orderBy = require('lodash.orderby')

module.exports = {
  getProductDisplay: async surveyId => {
    try {
      const getProductDisplayOrderList = await ProductDisplay.find({
        survey: surveyId
      })
      if (getProductDisplayOrderList.length) {
        const { id, products, count, displayType } = orderBy(
          getProductDisplayOrderList,
          'count',
          'asc'
        )[0]
        await ProductDisplay.updateOne({ id: id }).set({
          count: count + 1
        })
        if (displayType === 'randomise') {
          const randomise = (a, b) => 0.5 - Math.random()
          const newOrderProducts = products.sort(randomise)
          return {
            productDisplay: id,
            productDisplayOrder: newOrderProducts,
            productDisplayType: displayType
          }
        } else {
          return {
            productDisplay: id,
            productDisplayOrder: products,
            productDisplayType: displayType
          }
        }
      } else {
        return {
          productDisplay: null,
          productDisplayOrder: [],
          productDisplayType: 'none'
        }
      }
    } catch (error) {
      return {
        error: error
      }
    }
  }
}
