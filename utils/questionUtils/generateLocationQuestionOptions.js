const locationOptions = require('./locationOptions')

const generateLocationQuestionOptions = region => {
  if (!region) {
    throw new Error(
      'Region should be specified while creating Location question'
    )
  }
  if (!locationOptions[region]) {
    throw new Error('Cannot generate options for provided region')
  }
  return locationOptions[region]
}

module.exports = generateLocationQuestionOptions
