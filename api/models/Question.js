/**
 * Question.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */
const imageCreation = require('../../utils/imageCreation')

const imageCreationHook = async (values, cb) => {
  const { typeOfQuestion } = values
  if (typeOfQuestion === 'paired-questions') {
    const { pairsOptions } = values
    const { elementsInPairs } = pairsOptions
    values.pairsOptions.elementsInPairs =
      elementsInPairs &&
      elementsInPairs.length &&
      (await Promise.all(
        elementsInPairs.map(async p => {
          if (p.image800) {
            return {
              pair: p.pair,
              image800: await imageCreation(p.image800)
            }
          } else {
            return p
          }
        })
      ))
    cb()
  } else if (
    typeOfQuestion === 'choose-one' ||
    typeOfQuestion === 'choose-multiple'
  ) {
    const { options } = values
    values.options =
      options &&
      options.length &&
      (await Promise.all(
        options.map(async o => {
          if (o.image800) {
            return {
              ...o,
              image800: await imageCreation(o.image800)
            }
          } else {
            return o
          }
        })
      ))
    cb()
  } else {
    cb()
  }
}

module.exports = {
  attributes: {
    typeOfQuestion: {
      type: 'string',
      required: true,
      isIn: [
        'email',
        'vertical-rating',
        'choose-one',
        'taster-name',
        'open-answer',
        'paired-questions',
        'choose-multiple',
        'select-and-justify',
        'choose-product',
        'info',
        'upload-picture',
        'profile',
        'dropdown',
        'choose-date',
        'slider',
        'location',
        'numeric',
        'paypal-email',
        'time-stamp',
        'choose-payment',
        'matrix'
      ]
    },

    order: {
      type: 'number'
    },

    numericOptions: {
      type: 'json'
    },

    nextQuestion: {
      model: 'Question'
    },

    likingQuestion: {
      model: 'Question'
    },

    displayOn: {
      type: 'string',
      required: true,
      isIn: ['screening', 'begin', 'middle', 'end', 'payments']
    },

    requiredQuestion: {
      type: 'boolean',
      defaultsTo: true
    },

    eligibilityValues: {
      type: 'json'
    },

    prompt: {
      type: 'string'
    },

    secondaryPrompt: {
      type: 'string'
    },

    range: {
      type: 'json'
    },

    // options: contains posible values of the question
    // for example: all items of dropdown are options
    options: {
      type: 'json'
    },

    // settings: question settings
    // for example: minimum, maximum values that the taster can select on a question
    settings: {
      type: 'json'
    },

    optionDisplayType: {
      type: 'string',
      defaultsTo: 'labelOnly',
      isIn: ['labelOnly', 'imageOnly', 'imageAndLabel']
    },

    sliderOptions: {
      type: 'json'
    },
    matrixOptions: {
      type: 'json'
    },
    region: {
      type: 'string',
      isIn: ['us', 'de', 'uk', 'fr', 'fr-dep', 'br', 'ca']
    },

    skipFlow: {
      type: 'json',
      columnType: 'array'
    },

    addCustomOption: {
      type: 'boolean'
    },

    pairsOptions: {
      type: 'json'
    },

    answers: {
      collection: 'Answer',
      via: 'question'
    },

    survey: {
      model: 'Survey',
      required: true
    },

    relatedQuestions: {
      type: 'json'
    },

    chartTitle: {
      type: 'string'
    },

    chartTopic: {
      type: 'string'
    },

    hasPaymentOptions: {
      type: 'boolean',
      defaultsTo: false
    },
    showProductImage: {
      type: 'boolean',
      defaultsTo: true
    },
    chartType: {
      type: 'string',
      isIn: [
        'pie',
        'column',
        'columns-mean',
        'bar',
        'spider',
        'map',
        'table',
        'stacked-column',
        'stacked-column-horizontal-bars',
        'stacked-bar',
        'line',
        'horizontal-bars-mean',
        'spearmann-table',
        'pearson-table',
        'tags-list'
      ]
    }
  },
  beforeCreate: imageCreationHook,
  beforeUpdate: imageCreationHook,
  customToJSON: function () {
    const { settings, ...rest } = this
    return {
      ...rest,
      settings:
        (settings && {
          type: rest.typeOfQuestion,
          ...settings
        }) ||
        undefined
    }
  }
}
