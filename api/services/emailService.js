// eslint-disable-next-line node/no-extraneous-require
const Promise = require('bluebird')
const nodemailer = require('nodemailer')
const path = require('path')
const rp = require('request-promise')
const moment = require('moment')
const getDateString = require('../../utils/getDateString')
const generateProductsImagesHtml = require('../../utils/generateProductsImagesHtml')
const generateProductsNamesString = require('../../utils/generateProductsNamesString')
const smtpTransport = require('nodemailer-smtp-transport')
const readFile = Promise.promisify(require('fs').readFile)
const accessFile = Promise.promisify(require('fs').access)
const isDEV = process.env.NODE_ENV === 'development'
const getEmailTemplateFilePath = templateFileName =>
  path.join(__dirname, './email-templates', templateFileName)
const getEmailTemplateHtml = async templateFileName => {
  const filePath = getEmailTemplateFilePath(templateFileName)
  const htmlContent = await readFile(filePath, 'utf-8')
  return htmlContent
}
const generateSha256 = require('../../utils/signature')
const logger = require('../../utils/logger')
const i18n = require('../../utils/internationalization/i18n')
const querystring = require('querystring')
const { EMAIL_TEMPLATES } = require('../../utils/constants')

const emailConfig = {
  host: 'email-smtp.eu-west-1.amazonaws.com',
  port: 465,
  secureConnection: true,
  auth: {
    user: process.env.AWS_SES_USER, // Use from Amazon Credentials
    pass: process.env.AWS_SES_PASSWORD // Use from Amazon Credentials
  },
  logger: true
}

const flavorwikiLogo = {
  cid: 'flavorwiki',
  filename: 'flavorwiki.png',
  path: path.join(__dirname, './email-templates', 'flavorwiki.png')
}

const emailSubjects = {
  requestAccountRequester: 'Thank you for requesting an account',
  requestAccountReviewer: {
    operator: 'New OPERATOR account request',
    taster: 'New TASTER account request'
  },
  requestNewAccount: {
    operator: 'An operator wants to request a taster account.',
    taster: 'A taster wants to request an operator account.'
  }
}

const emailFileName = {
  requestAccountRequester: 'requestAccountRequester.html',
  requestAccountReviewer: 'requestAccountReviewer.html',
  requestNewAccount: {
    operator: 'requestAccountTaster.html',
    taster: 'requestAccountOperator.html'
  }
}

const buildEmailBody = async (content, opt = {}) => {
  const lang = opt.language || 'en'
  const t = i18n.getFixedT(lang)
  const htmlFilePath = path.join(
    __dirname,
    `./email-templates/emailMasterTemplate.html`
  )

  try {
    await accessFile(htmlFilePath)
    const templateContent = await readFile(htmlFilePath, 'utf-8')
    return templateContent
      .replace(/{{ privacyPolicy }}/g, t('privacyPolicy'))
      .replace(/{{ message }}/g, content)
  } catch (ex) {
    logger.error('Error when sending the email.', ex)
    throw ex
  }
}

var transporter = nodemailer.createTransport(smtpTransport(emailConfig))

const sendEmail = ({
  attachments = [],
  receiver,
  emailVars = {},
  template,
  toReturn = {}
}) => {
  try {
    const url = '/api/v1/send/'
    const apiKey = process.env.PEPOCLIENT_API_KEY
    const apiSecret = process.env.PEPOCLIENT_SECRET_KEY
    const requestTime = moment().format()
    const delimiter = '::'
    const string = url + delimiter + requestTime
    const signature = generateSha256(apiSecret, string)
    const body = {
      'request-time': requestTime,
      'signature': signature,
      'api-key': apiKey,
      'email': receiver,
      'template': template,
      'email_vars': JSON.stringify(emailVars)
    }

    const options = {
      method: 'POST',
      uri: 'https://pepocampaigns.com/api/v1/send?' + querystring.stringify(body),
      json: true
    }
    rp(options)
      .then(function (parsedBody) {
        console.log('dajknsndkjsanknakd', parsedBody)
      })
      .catch(function (err) {
        console.log('dbsjabjd=====> error', err)
      })
  } catch (error) {
    console.log('error', error)
  }
}
const sendDirectEmail = ({
  attachments = [],
  receiver,
  html,
  subject,
  toReturn = {}
}) => {
  const sender = isDEV
    ? 'Alexandru <alexandru@flavorwiki.com>'
    : 'Flavorwiki <noreply@flavorwiki-tasting.com>'
  if (html.then) {
    throw new Error('expected html is a string, but it is a Promise')
  }
  return new Promise((resolve, reject) => {
    transporter.sendMail(
      {
        from: sender,
        to: receiver,
        subject,
        replyTo: 'Flavorwiki <tasting@flavorwiki.com>',
        html,
        attachments
      },
      err => {
        if (err) reject(err)
        resolve(toReturn)
      }
    )
  })
}

module.exports = {
  sendEmail,
  buildEmailBody,
  sendSurveyWaitingMail: async ({
    email,
    productsNames,
    productsImages,
    url,
    language = 'en',
    shareLink,
    amount,
    allowedDaysToFillTheTasting,
    surveyEmail
  }) => {
    if (
      surveyEmail &&
      surveyEmail.toSend !== undefined &&
      !surveyEmail.toSend
    ) {
      return
    }

    return sendEmail({
      receiver: email,
      template: EMAIL_TEMPLATES[language]['sendSurveyWaitingMail'],
      emailVars: {
        share_link: shareLink,
        share_amount: amount,
        allowed_days: allowedDaysToFillTheTasting,
        privacyPolicy: 'Privacy Policy'
      },
      attachments: []
    })
  },
  sendSurveyCompletionMail: async ({
    email,
    shareLink,
    language = 'en',
    amount,
    surveyEmail,
    allowedDaysToFillTheTasting
  }) => {
    if (
      surveyEmail &&
      surveyEmail.toSend !== undefined &&
      !surveyEmail.toSend
    ) {
      return
    }

    // We might need these if we decide on returning to creating template from application
    // const subject = surveyEmail.subject
    // const html = surveyEmail.html
    //   .replace(/__SHARE_LINK__/g, shareLink)
    //   .replace(/__SHARE_AMOUNT__/g, amount)
    // const text = surveyEmail.text
    //   .replace(/__SHARE_LINK__/g, shareLink)
    //   .replace(/__SHARE_AMOUNT__/g, amount)
    return sendEmail({
      receiver: email,
      template: EMAIL_TEMPLATES[language]['sendSurveyCompletionMail'],
      emailVars: {
        share_link: shareLink,
        share_amount: amount,
        allowed_days: allowedDaysToFillTheTasting
      },
      attachments: []
    })
  },
  sendSurveyRejectionMail: async ({
    email,
    shareLink,
    language = 'en',
    amount,
    surveyEmail
  }) => {
    if (
      surveyEmail &&
      surveyEmail.toSend !== undefined &&
      !surveyEmail.toSend
    ) {
      return
    }
    return sendEmail({
      receiver: email,
      template: EMAIL_TEMPLATES[language]['sendSurveyRejectionMail'],
      emailVars: {
        share_link: shareLink,
        share_amount: amount
      },
      attachments: []
    })
  },
  sendOperatorRequestTasterAccountEmail: async ({
    adminEmail,
    emailAddress
  }) => {
    const subject = emailSubjects.requestNewAccount.operator
    const dateString = getDateString(new Date(Date.now()))
    const htmlFileName = emailFileName.requestNewAccount.operator
    const emailTemplateHtml = await getEmailTemplateHtml(htmlFileName)
    const attachments = [flavorwikiLogo]
    const html = emailTemplateHtml
      .replace('{{ date }}', dateString)
      .replace(/{{ email }}/g, emailAddress)

    sendEmail({ receiver: adminEmail, html, subject, attachments })
    sendEmail({
      receiver: 'software@flavorwiki.com',
      html,
      subject,
      attachments
    })
  },
  sendTasterRequestOperatorAccountEmail: async ({
    adminEmail,
    emailAddress
  }) => {
    const subject = emailSubjects.requestNewAccount.taster
    const dateString = getDateString(new Date(Date.now()))
    const htmlFileName = emailFileName.requestNewAccount.taster
    const emailTemplateHtml = await getEmailTemplateHtml(htmlFileName)
    const attachments = [flavorwikiLogo]
    const html = emailTemplateHtml
      .replace('{{ date }}', dateString)
      .replace(/{{ email }}/g, emailAddress)

    sendEmail({ receiver: adminEmail, html, subject, attachments })
    sendEmail({
      receiver: 'software@flavorwiki.com',
      html,
      subject,
      attachments
    })
  },
  sendRequestAccountEmail: async ({
    requesterEmail,
    userType,
    firstName,
    lastName,
    companyName,
    phoneNumber
  }) => {
    const dateString = getDateString(new Date(Date.now()))

    const requesterSubject = emailSubjects.requestAccountRequester
    const reviewerSubject =
      emailSubjects.requestAccountReviewer[userType || 'taster'] // fallback to taster account in case of error

    const requesterEmailTemplateFileName = emailFileName.requestAccountRequester
    const reviewerEmailTemplateFileName = emailFileName.requestAccountReviewer

    const requesterEmailTemplateHtml = await getEmailTemplateHtml(
      requesterEmailTemplateFileName
    )
    const reviewerEmailTemplateHtml = await getEmailTemplateHtml(
      reviewerEmailTemplateFileName
    )

    const attachments = [flavorwikiLogo]

    const requesterHtml = requesterEmailTemplateHtml
      .replace('{{ date }}', dateString)
      .replace('{{ firstName }}', firstName)

    const reviewerHtml = reviewerEmailTemplateHtml
      .replace('{{ date }}', dateString)
      .replace('{{ userType }}', userType)
      .replace(
        '{{ userTypeCaps }}',
        userType.charAt(0).toUpperCase() + userType.slice(1).toLowerCase()
      )
      .replace('{{ firstName }}', firstName)
      .replace('{{ lastName }}', lastName)
      .replace('{{ companyName }}', companyName)
      .replace('{{ email }}', requesterEmail)
      .replace('{{ phoneNumber }}', phoneNumber)

    sendEmail({
      receiver: requesterEmail,
      html: requesterHtml,
      subject: requesterSubject,
      attachments
    })
    sendEmail({
      receiver: 'software@flavorwiki.com',
      html: reviewerHtml,
      subject: reviewerSubject,
      attachments
    })
  },
  sendWelcomeEmail: async ({ email, language = 'en' }) => {
    const t = i18n.getFixedT(language)
    // const subject = t('newAccount.subject')
    const template = t('newAccount.template')
    sendEmail({
      receiver: email,
      emailVars: {},
      template,
      attachments: []
    })
  },
  sendResetPasswordEmail: async ({ email, resetLink, language = 'en' }) => {
    const t = i18n.getFixedT(language)
    const subject = t('resetPassword.subject')
    const html = t('resetPassword.content').replace(
      /__RESET_LINK__/g,
      resetLink
    )
    sendDirectEmail({
      receiver: email,
      html: await buildEmailBody(html, { language }),
      subject,
      attachments: []
    })
  },
  sendVerificationEmail: async ({ email, verifyLink, language = 'en' }) => {
    const t = i18n.getFixedT(language)
    const subject = t('verifyEmail.subject')
    const html = t('verifyEmail.content').replace(
      /__VERIFY_LINK__/g,
      verifyLink
    )
    sendDirectEmail({
      receiver: email,
      html: await buildEmailBody(html, { language }),
      subject,
      attachments: []
    })
  }
}
