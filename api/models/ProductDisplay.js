/**
 * ProductDisplay.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    survey: {
      model: 'Survey',
      required: true
    },
    products: {
      type: 'json',
      columnType: 'array'
    },
    displayType: {
      type: 'string',
      isIn: ['reverse', 'permutation', 'forced', 'none'],
      defaultsTo: 'none'
    },
    state: {
      type: 'string',
      isIn: ['active', 'inActive'],
      defaultsTo: 'active'
    },
    count: {
      type: 'number',
      defaultsTo: 0
    }
  }
}
