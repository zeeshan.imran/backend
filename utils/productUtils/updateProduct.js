/* globals Product, Brand */
const s3Utils = require('../../utils/s3Utils')

module.exports = async (id, productInput) => {
  const {
    name,
    photo,
    brand: brandName,
    reward,
    isAvailable,
    isSurveyCover,
    sortingOrderId
  } = productInput

  const brand = await Brand.findOrCreate(
    { name: brandName },
    { name: brandName }
  )

  // TODO: STAGFW-455 tested
  const product = await Product.findOne({ id })
  const shouldResizePhoto = product.photo !== photo

  if (shouldResizePhoto) {
    const s3Key = s3Utils.getObjectKey(photo)
    if (s3Key) {
      await s3Utils.resizePhoto(s3Key)
    }
  }

  const updatedProduct = await Product.updateOne({
    id
  }).set({
    name,
    photo,
    brand: brand.id,
    reward,
    isAvailable,
    isSurveyCover: Boolean(isSurveyCover),
    sortingOrderId
  })

  return updatedProduct
}
