/* globals Organization, User, Survey, Question, Answer, SurveyEnrollment */
/* eslint-env jest */
const sails = require('sails')
const supertest = require('supertest')
const { organizations, users, surveys, questions } = require('./fixtures')

const startSails = () =>
  new Promise((resolve, reject) => {
    sails.lift(
      { hooks: { 'sails-hook-apianalytics': false }, log: { level: 'warn' } },
      async function (err) {
        if (err) {
          return reject(err)
        }

        global.sails = sails
        global.app = supertest(sails.hooks.http.app)
        resolve()
      }
    )
  })

const shutdownSails = () => new Promise(resolve => sails.lower(resolve))

beforeAll(async () => {
  await startSails()
  await Organization.createEach(organizations).fetch()
  await User.createEach(users).fetch()
  await Survey.createEach(surveys).fetch()
  await Question.createEach(questions).fetch()
  // eslint-disable-next-line no-console
  console.log('test server was started!')
}, 60000)

afterAll(async () => {
  await Organization.destroy({})
  await User.destroy({})
  await Answer.destroy({})
  await Question.destroy({})
  await SurveyEnrollment.destroy({})
  await Survey.destroy({})
  await shutdownSails()
})
