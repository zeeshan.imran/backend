/* globals sails, Survey, SurveyEnrollment, Question, Answer, User, emailService */
/**
 * SurveyEnrollmentController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const rp = require('request-promise')
const R = require('ramda')
const ApiError = require('../../utils/ApiError')
const { answerSearch } = require('../services/searches')
const { getScreenerSurveys } = require('../services/surveyService')

const canChangeSurveyState = ({ from, to }) => {
  const wantsToRollbackFinished = from === 'finished' && to !== 'paid'
  const inTerminalState = from === 'paid' || from === 'rejected'

  return !wantsToRollbackFinished && !inTerminalState
}

const updateLinkedSurveys = async surveyEnrollment => {
  const { survey, user } = surveyEnrollment
  const mainSurvey = await Survey.findOne({ id: survey }).populate(
    'linkedSurveys'
  )
  if (mainSurvey) {
    const { linkedSurveys, isScreenerOnly } = mainSurvey
    if (isScreenerOnly && linkedSurveys.length) {
      const linkSurveyIds = R.pluck('id', linkedSurveys)
      await Survey.addToCollection(
        R.uniq(linkSurveyIds),
        'exclusiveTasters'
      ).members(user)
    }
  }
}

const sendMatrixStats = async (surveyId, respondentId, matrixAnswers) => {
  const matrixAnswersByQuestion = R.groupBy(R.prop('question'), matrixAnswers)
  for (let questionId in matrixAnswersByQuestion) {
    const matrixAnswersByProductQuestion = R.groupBy(
      R.prop('product'),
      matrixAnswersByQuestion[questionId]
    )
    for (let productId in matrixAnswersByProductQuestion) {
      const answers = R.flatten(
        matrixAnswersByProductQuestion[productId].map(answer => {
          const matrixValues = answer.value[0]
          return Object.keys(matrixValues).map(row => ({
            row,
            rowValues: Array.isArray(matrixValues[row])
              ? matrixValues[row] // send multiple values
              : [matrixValues[row]] // send single value as array with single item
          }))
        })
      )
      try {
        await rp({
          method: 'POST',
          uri: `${
            sails.config.custom.statsApi
          }/survey/${surveyId}/respondent/${respondentId}/matrix/${questionId}/${productId}`,

          body: answers,
          json: true
        })
      } catch (error) {
        // eslint-disable-next-line
        console.log('<===== Matrix Error ====>', error)
      }
    }
  }
}

const sendSurveyStats = async surveyEnrollment => {
  const questionsAnsweredIds = surveyEnrollment.answers.map(a => a.question)
  const questionsAnswered = await Question.find({
    id: questionsAnsweredIds
  })
  const questionsById = questionsAnswered.reduce(
    (acc, q) => Object.assign({}, acc, { [q.id]: q }),
    {}
  )

  const filteredAnswers = surveyEnrollment.answers.filter(
    answer =>
      questionsById[answer.question] &&
      questionsById[answer.question].typeOfQuestion !== 'paired-questions' &&
      questionsById[answer.question].typeOfQuestion !== 'slider' &&
      questionsById[answer.question].typeOfQuestion !== 'matrix'
  )

  let flatAnswers = filteredAnswers.reduce((flat, answer) => {
    const flatAnswer = answer.value.map(value =>
      // Answer can be a plain string or object
      Object.assign(
        {},
        answer,
        typeof value === 'object' ? value : { value: value }
      )
    )
    return [...flat, ...flatAnswer]
  }, [])
  let flatItems = []
  flatAnswers.map(item => {
    if (item.value === 'other' && item.desc) {
      let copyItem = Object.assign({}, item)
      copyItem.value = item.desc
      flatItems.push(copyItem)
    }
  })
  flatAnswers = [...flatItems, ...flatAnswers]
  const answersByProduct = flatAnswers.reduce((acc, answer) => {
    const targetProduct = answer.product || 'no-product'

    if (
      answer.value !== '' &&
      answer.value !== null &&
      answer.value !== undefined
    ) {
      acc[targetProduct] = [
        ...(acc[targetProduct] || []),
        {
          answer_id: answer.id,
          question_id: answer.question,
          answer_value: answer.value,
          answer_desc: answer.desc
        }
      ]
    }

    return acc
  }, {})

  const productsWithAnswers = Object.keys(answersByProduct).map(productId => {
    let formattedProductId = productId === 'no-product' ? null : productId
    return {
      product_id: formattedProductId,
      data: answersByProduct[productId]
    }
  })

  const surveyId = surveyEnrollment.survey
  const respondentId = surveyEnrollment.id

  rp({
    method: 'POST',
    uri: `${
      sails.config.custom.statsApi
    }/survey/${surveyId}/respondent/${respondentId}`,
    body: productsWithAnswers,
    json: true
  })

  sendMatrixStats(
    surveyId,
    respondentId,
    surveyEnrollment.answers.filter(
      answer => questionsById[answer.question].typeOfQuestion === 'matrix'
    )
  )
}

module.exports = {
  saveRewards: async (req, res) => {
    try {
      const {
        surveyEnrollment: surveyEnrollmentId,
        rewards: newRewards
      } = req.body

      await SurveyEnrollment.update({
        id: surveyEnrollmentId
      }).set({
        savedRewards: newRewards
      })
      return res.ok(true)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  submitForcedSignUp: async (req, res) => {
    try {
      const { surveyEnrollment, email, password, country } = req.body

      if (!surveyEnrollment || !email) {
        return res.badRequest('Something went wrong')
      }

      let user = await User.findOne({ emailAddress: email.toLowerCase() })

      if (!user) {
        user = await User.create({
          emailAddress: email,
          password: password,
          country: country,
          type: 'taster',
          isTaster: true
        }).fetch()
        await emailService.sendWelcomeEmail({
          email,
          language: user.language
        })
      } else {
        if (password && country) {
          const updatedUser = await User.updateOne({ id: user.id }).set({
            password,
            country,
            type: user.type === 'respondent' ? `taster` : user.type
          })
          if (
            updatedUser &&
            updatedUser.password &&
            updatedUser.password !== ''
          ) {
            await emailService.sendWelcomeEmail({
              email,
              language: user.language
            })
          }
        }
      }

      await SurveyEnrollment.updateOne({
        id: surveyEnrollment
      }).set({
        user: user.id
      })

      return res.ok()
    } catch (error) {
      return res.badRequest(error)
    }
  },
  submitAnswer: async (req, res) => {
    try {
      const {
        question,
        surveyEnrollment,
        value,
        startedAt,
        selectedProduct: product
      } = req.body
      // await Answer.destroy({ id: question, enrollment: surveyEnrollment })
      const targetQuestion = await Question.findOne({ id: question }).populate(
        'survey'
      )
      const survey = targetQuestion.survey

      if (!['active', 'draft'].includes(survey.state)) {
        throw new ApiError('E_SURVEY_IS_NOT_ACTIVE', 'The survey is not active')
      }

      const { typeOfQuestion } = targetQuestion
      let answersToCreate = []

      if (typeOfQuestion === 'paypal-email' && value[0]) {
        const updatedSurveyEnrollment = await SurveyEnrollment.update({
          id: surveyEnrollment
        })
          .set({
            paypalEmail: value[0]
          })
          .fetch()
        if (survey.isScreenerOnly && updatedSurveyEnrollment.length) {
          const user = await User.update({
            id: updatedSurveyEnrollment[0].user
          })
            .set({
              paypalEmailAddress: value[0]
            })
            .fetch()
        }
      }

      if (typeOfQuestion === 'email' && value[0]) {
        if (survey.authorizationType === 'public') {
          let user = await User.findOne({ emailAddress: value[0] })
          if (!user) {
            user = await User.create({ emailAddress: value[0] }).fetch()
          }
          await SurveyEnrollment.update({
            id: surveyEnrollment
          }).set({
            user: user.id
          })
        }
      }

      let accTimeToAnswer = []
      switch (typeOfQuestion) {
        case 'choose-product':
          await SurveyEnrollment.addToCollection(
            surveyEnrollment,
            'selectedProducts',
            value
          )
          await SurveyEnrollment.updateOne({ id: surveyEnrollment }).set({
            lastSelectedProduct: value && value.length ? value[0] : ''
          })
          answersToCreate = []
          break
        case 'info':
        case 'profile':
          answersToCreate = []
          break
        case 'paypal-email':
          answersToCreate[0] = {
            question,
            startedAt,
            timeToAnswer: Date.now() - new Date(startedAt).getTime(),
            enrollment: surveyEnrollment,
            value
          }
          break
        case 'paired-questions':
          if (value) {
            answersToCreate = value.map((pair, index) => {
              const { pairQuestion, value: pairValue, timeToAnswer } = pair
              accTimeToAnswer[index] =
                timeToAnswer + (accTimeToAnswer[index - 1] || 0)
              const startedPairAt = startedAt + accTimeToAnswer[index]

              return {
                question,
                startedAt: startedPairAt,
                timeToAnswer,
                enrollment: surveyEnrollment,
                pairQuestion,
                product,
                value: [pairValue]
              }
            })
          }

          break
          
        default:
          answersToCreate[0] = {
            question,
            startedAt,
            timeToAnswer: Date.now() - new Date(startedAt).getTime(),
            enrollment: surveyEnrollment,
            value,
            product
          }
          break
      }

      if (
        !question.required &&
        answersToCreate.length &&
        answersToCreate[0].value === undefined
      ) {
        await SurveyEnrollment.updateOne({ id: surveyEnrollment }).set({
          lastAnsweredQuestion: question
        })
        return res.ok([])
      }

      const answers = await Answer.createEach(answersToCreate).fetch()
      await SurveyEnrollment.updateOne({ id: surveyEnrollment }).set({
        lastAnsweredQuestion: question
      })
      return res.ok(answers)
    } catch (error) {
      console.log('<=====Found Error====>', error)
      return res.badRequest(error)
    }
  },
  setSurveySelectedProducts: async (req, res) => {
    try {
      const { surveyEnrollment, selectedProducts } = req.body
      await SurveyEnrollment.updateOne({
        id: surveyEnrollment
      }).set({
        selectedProducts
      })
      const updatedSurveyEnrollment = await SurveyEnrollment.findOne({
        id: surveyEnrollment
      }).populate('selectedProducts')

      return res.ok(updatedSurveyEnrollment)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  setPaypalEmail: async (req, res) => {
    try {
      const { surveyEnrollment, paypalEmail } = req.body
      const updatedSurveyEnrollment = await SurveyEnrollment.updateOne({
        id: surveyEnrollment
      }).set({
        paypalEmail
      })
      return res.ok(updatedSurveyEnrollment)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  setState: async (req, res) => {
    try {
      const { surveyEnrollment, state } = req.body
      const { state: currentState } = await SurveyEnrollment.findOne({
        id: surveyEnrollment
      })
      if (!canChangeSurveyState({ from: currentState, to: state })) {
        return res.forbidden()
      }
      const updatedSurveyEnrollment = await SurveyEnrollment.updateOne({
        id: surveyEnrollment
      }).set({
        state,
        finishedAt: state === 'finished' ? new Date() : ''
      })
      return res.ok(updatedSurveyEnrollment)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  sendSurveyStats: async (req, res) => {
    const { surveyEnrollment: surveyEnrollmentId } = req.body

    const surveyEnrollment = await SurveyEnrollment.findOne({
      id: surveyEnrollmentId
    }).populate('answers')
    await sendSurveyStats(surveyEnrollment)

    return res.ok()
  },
  finishSurvey: async (req, res) => {
    try {
      const { surveyEnrollment: surveyEnrollmentId } = req.body
      const surveyEnrollment = await SurveyEnrollment.findOne({
        id: surveyEnrollmentId
      }).populate('answers')

      if (
        !canChangeSurveyState({ from: surveyEnrollment.state, to: 'finished' })
      ) {
        return res.forbidden()
      }

      await SurveyEnrollment.updateOne({
        id: surveyEnrollmentId
      }).set({
        state: 'finished',
        finishedAt: new Date()
      })

      await sendSurveyStats(surveyEnrollment)

      return res.ok()
    } catch (error) {
      return res.badRequest(error)
    }
  },
  rejectSurvey: async (req, res) => {
    try {
      const { surveyEnrollment: surveyEnrollmentId } = req.body
      const surveyEnrollment = await SurveyEnrollment.findOne({
        id: surveyEnrollmentId
      }).populate('answers')

      if (
        !canChangeSurveyState({ from: surveyEnrollment.state, to: 'rejected' })
      ) {
        return res.forbidden()
      }
      await SurveyEnrollment.updateOne({
        id: surveyEnrollmentId
      }).set({
        state: 'rejected',
        finishedAt: new Date()
      })

      return res.ok()
    } catch (error) {
      return res.badRequest(error)
    }
  },
  finishSurveyScreening: async (req, res) => {
    try {
      const { surveyEnrollment: surveyEnrollmentId } = req.body
      const surveyEnrollment = await SurveyEnrollment.findOne({
        id: surveyEnrollmentId
      })
      console.log('===dsnakdnkasndkjnasjdkasd',canChangeSurveyState({
        from: surveyEnrollment.state,
        to: 'waiting-for-product'
      }))
      if (
        !canChangeSurveyState({
          from: surveyEnrollment.state,
          to: 'waiting-for-product'
        })
      ) {
        return res.forbidden()
      }

      await SurveyEnrollment.updateOne({
        id: surveyEnrollmentId
      }).set({
        state: 'waiting-for-product'
      })
      await updateLinkedSurveys(surveyEnrollment)

      return res.ok()
    } catch (error) {
      return res.badRequest(error)
    }
  },
  getSurveyEnrollementValidation: async (req, res) => {
    try {
      const { query } = req.query
      const params = JSON.parse(query)
      const { surveyId } = params

      if (!surveyId) return res.badRequest('Not Survey Id Provided')

      const survey = await Survey.findOne({ id: surveyId })
      let hasScreener = false

      if (!survey) return res.badRequest('Not Survey Found')

      let filterClause = {
        survey: surveyId,
        state: 'finished'
      }
      if (surveyId) {
        const checkScreener = await getScreenerSurveys(surveyId)
        hasScreener = checkScreener.length > 0
      }

      const surveyEnrollements = await SurveyEnrollment.find(filterClause)
        .populate('user')
        .sort('createdAt desc')

      if (!surveyEnrollements) {
        return res.ok({
          total: 0,
          enrollments: []
        })
      }
      const total = surveyEnrollements.length

      const questions = await Question.find({
        survey: surveyId,
        typeOfQuestion: 'upload-picture'
      })
      if (questions.length) {
        const surveyEnrollementIds = R.pluck('id', surveyEnrollements)
        const questionIds = R.pluck('id', questions)
        const answers = await Answer.find({
          where: {
            question: { in: questionIds },
            enrollment: { in: surveyEnrollementIds }
          }
        })
          .populate('product')
          .populate('question')
        const groupByEnrollement = R.groupBy(answer => {
          return answer.enrollment
        })
        const answersByEnrollment = groupByEnrollement(answers)
        const enrollments = surveyEnrollements.filter(singleEnrollment => {
          if (answersByEnrollment[singleEnrollment.id]) {
            singleEnrollment['answers'] =
              answersByEnrollment[singleEnrollment.id]
            if (hasScreener) {
              singleEnrollment['paypalEmail'] =
                singleEnrollment.user.paypalEmailAddress
            }
            return singleEnrollment
          }
        })
        return res.ok({
          total,
          enrollments
        })
      }

      return res.ok({
        total: 0,
        enrollments: []
      })
    } catch (error) {
      return res.badRequest(error)
    }
  },
  getSurveyFunnel: async (req, res) => {
    try {
      const { query } = req.query
      const params = JSON.parse(query)
      const { surveyId, currentPage, perPage } = params
      if (!surveyId) return res.badRequest('Not Survey Id Provided')

      const survey = await Survey.findOne({ id: surveyId })

      if (!survey) return res.badRequest('Not Survey Found')

      const skipValue = (currentPage - 1) * perPage

      const total = await Question.count({
        survey: surveyId
      })

      const funnel = []

      const questions = await Question.find({
        where: {
          survey: surveyId
        },
        select: ['id', 'prompt']
      })
        .sort('order ASC')
        .skip(skipValue)
        .limit(perPage)

      if (questions.length) {
        const answers = await answerSearch.funnel(questions)
        if (answers) {
          const groupByQuestions = R.groupBy(answer => {
            return answer.question
          })
          const answersByQuestions = groupByQuestions(answers)
          questions.map(value => {
            funnel.push({
              question: value,
              product:
                answersByQuestions[value.id] &&
                answersByQuestions[value.id][0].product[0].id
                  ? answersByQuestions[value.id][0].product
                  : null,
              responses: answersByQuestions[value.id]
                ? answersByQuestions[value.id][0].responses
                : 0
            })
          })
        }
        return res.ok({
          total,
          funnel
        })
      }

      return res.ok({
        total: 0,
        funnel: []
      })
    } catch (error) {
      return res.badRequest(error)
    }
  },
  updateSurveyEnrollementValidation: async (req, res) => {
    try {
      const { user: currentUserToken } = req.token
      const currentUser = await User.findOne({ id: currentUserToken.id })

      if (!currentUser.isSuperAdmin) {
        throw new Error('Not alowed')
      }

      const params = req.body
      const updated = await SurveyEnrollment.updateOne({ id: params.id }).set({
        comment: params.comment ? params.comment : '',
        validation: params.validation,
        processed: params.processed
      })
      return res.ok(updated)
    } catch (error) {
      return res.badRequest(error)
    }
  },

  updateSurveyEnrollementProductIncentives: async (req, res) => {
    try {
      const { user: currentUserToken } = req.token
      const currentUser = await User.findOne({ id: currentUserToken.id })

      if (!currentUser.isSuperAdmin) {
        throw new Error('Not alowed')
      }

      const params = req.body
      const updated = await SurveyEnrollment.update({
        survey: params.surveyID,
        id: { in: params.enrollments }
      })
        .set({
          productIncentiveExported: true
        })
        .fetch()
      return res.ok({ updatedRecords: updated.length })
    } catch (error) {
      return res.badRequest(error)
    }
  },
  updateSurveyEnrollementGiftCardIncentives: async (req, res) => {
    try {
      const { user: currentUserToken } = req.token
      const currentUser = await User.findOne({ id: currentUserToken.id })

      if (!currentUser.isSuperAdmin) {
        throw new Error('Not alowed')
      }

      const params = req.body
      const updated = await SurveyEnrollment.update({
        survey: params.surveyID,
        id: { in: params.enrollments }
      })
        .set({
          giftCardIncentiveExported: true
        })
        .fetch()
      return res.ok({ updatedRecords: updated.length })
    } catch (error) {
      return res.badRequest(error)
    }
  },
  updateSurveyShareStatus: async (req, res) => {
    try {
      const { user: currentUserToken } = req.token
      const currentUser = await User.findOne({ id: currentUserToken.id })

      if (!currentUser.isSuperAdmin) {
        throw new Error('Not alowed')
      }

      const params = req.body
      const updated = await SurveyEnrollment.update({
        survey: params.surveyID,
        id: { in: params.enrollments }
      })
        .set({
          shareExported: true
        })
        .fetch()
      const userIds = R.pluck('user', updated)
      if (userIds.length) {
        await User.update({ id: { in: userIds } })
          .set({
            completedReferredSurvey: true
          })
          .fetch()
      }
      return res.ok({ updatedRecords: updated.length })
    } catch (error) {
      return res.badRequest(error)
    }
  }
}
