const { createId } = require('../utils')

const organizations = [
  {
    id: createId(),
    name: 'Organization 1',
    uniqueName: 'organization-1'
  },
  {
    id: createId(),
    name: 'Organization 2',
    uniqueName: 'organization-2'
  }
]

module.exports = organizations
