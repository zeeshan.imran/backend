const fs = require('fs')
const util = require('util')
const access = util.promisify(fs.access)
const mkdir = util.promisify(fs.mkdir)
const writeFile = util.promisify(fs.writeFile)

const exists = async path => {
  try {
    await access(path)
    return true
  } catch (ex) {
    return false
  }
}

const createImagesFolder = async () => {
  const assets = './assets'
  if (!(await exists(assets))) {
    await mkdir(assets)
  }
  const images = './assets/images'
  if (!(await exists(images))) {
    await mkdir(images)
  }
  const questions = './assets/images/questions'
  if (!(await exists(questions))) {
    await mkdir(questions)
  }
}

const imageCreation = async base64 => {
  await createImagesFolder()
  if (base64.includes('base64')) {
    const ImageType = base64.match(/[^:]\w+\/[\w-+\d.]+(?=;|,)/)[0].split('/')
    const base64Data = base64.replace(/^data:([A-Za-z-+/]+);base64,/, '')
    const filePath = `${Math.random()
      .toString(36)
      .substring(7)}${Date.now()}.${ImageType[1]}`
    await writeFile(`assets/images/questions/${filePath}`, base64Data, {
      encoding: 'base64'
    })
    return `/images/questions/${filePath}`
  } else {
    return base64
  }
}

module.exports = imageCreation
