/* global PairQuestion */
const Combinatorics = require('js-combinatorics')

const generateCombinations = (pairs, questionId) =>
  Combinatorics.combination(pairs, 2)
    .toArray()
    .reduce(
      (permutations, combination) => [
        ...permutations,
        {
          leftAttribute: combination[0],
          rightAttribute: combination[1],
          question: questionId
        },
        {
          leftAttribute: combination[1],
          rightAttribute: combination[0],
          question: questionId
        }
      ],
      []
    )

module.exports = async (pairs, questionId) => {
  const combinations = generateCombinations(pairs, questionId)
  return PairQuestion.createEach(combinations)
}
