/**
 * ProductController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const { createProduct, updateProduct } = require('../../utils/productUtils')

module.exports = {
  create: async (req, res) => {
    try {
      const createdProduct = await createProduct(req.body)
      return res.ok(createdProduct)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  update: async (req, res) => {
    try {
      const updatedProduct = await updateProduct(req.params.id, req.body)

      return res.ok(updatedProduct)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  replaceProducts: async (req, res) => {
    const { products } = req.body
    const payload = []
    const productsWithIds = products.map((product, index) => {
      if (!product.sortingOrderId) {
        return {
          ...product,
          sortingOrderId: index + 1
        }
      } else {
        return product
      }
    })

    for (let { clientGeneratedId, ...product } of productsWithIds) {
      if (product.id) {
        const { id, ...rest } = product
        payload.push({
          product: await updateProduct(id, rest)
        })
      } else {
        payload.push({
          clientGeneratedId,
          product: await createProduct(product)
        })
      }
    }

    res.send(payload)
  }
}
