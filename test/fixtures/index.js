module.exports = {
  organizations: require('./organizations'),
  users: require('./users'),
  surveys: require('./surveys'),
  questions: require('./questions')
}
