// createShareLink({
//   shareId: 'hello-fresh-roast-dinner-171226',
//   title: 'HelloFresh Roast Dinner - FlavorWiki',
//   description: 'Thanks for your interest in taking "HelloFresh Roast Dinner"',
//   targetLink: '/survey/hello-fresh-roast-dinner-171226'
// })
// eslint-disable-next-line node/no-extraneous-require
const Promise = require('bluebird')
const path = require('path')
const readFile = Promise.promisify(require('fs').readFile)
const s3Utils = require('../../utils/s3Utils')
const escapeHtml = require('../../utils/escapeHtml')

const getHtmlTemplateContent = async templateFileName => {
  const filePath = path.join(__dirname, './html-templates', templateFileName)
  const htmlContent = await readFile(filePath, 'utf-8')
  return htmlContent
}

/**
 * We can create any share link by this tool
 * - the share link will be /s/{{shareId}}
 * - when user share this link, the title, description will be used to generate the social's widget (FB, Twitter, LinkedIn)
 * - when the user click on the share link, it will redirect the user to the targetLink
 * @param {Object} data - Share information
 * @param {string} data.shareId - Id of link to share.
 * @param {string} data.title - Title of share content
 * @param {string} data.description - Describe about the share content
 * @param {string} data.targetLink - The link of content (a survey link...)
 */

const createShareLink = async data => {
  let template = await getHtmlTemplateContent('share-survey.html')

  for (let prop of ['shareId', 'title', 'description', 'targetLink']) {
    template = template.replace(RegExp(`{{${prop}}}`, 'g'), data[prop])
  }

  await s3Utils.uploadFile(`s/${data.shareId}`, template, {
    ContentType: 'text/html'
  })
}

module.exports.createShareLink = createShareLink

// this function is not throw exception
module.exports.createSurveyShareLink = async survey => {
  try {
    await createShareLink({
      shareId: `survey/${survey.uniqueName}`,
      targetLink: `${process.env.APP_URL}/survey/${survey.id}`,
      title: `${survey.name} - FlavorWiki`,
      description: `Thanks for your interest in taking ${escapeHtml(
        survey.name
      )}`
    })
  } catch (ex) {
    //
  }
}
