var crypto = require('crypto');

const generateSha256 = (key, string) => {
  return crypto.createHmac('sha256', key).update(string).digest('hex')
}
module.exports = generateSha256
