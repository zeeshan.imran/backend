module.exports = function (req, res, next) {
  if (!req.token) {
    return res.forbidden('Not authorized')
  }
  const { user } = req.token

  if (user.type === 'power-user') {
    return next()
  } else {
    res.forbidden('Not authorized')
  }
}
