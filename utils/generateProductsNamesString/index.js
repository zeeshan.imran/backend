const generateProductsNamesString = productsNames =>
  [
    productsNames.slice(0, productsNames.length - 1).join(', '),
    productsNames[productsNames.length - 1]
  ].join(' and ')

module.exports = generateProductsNamesString
