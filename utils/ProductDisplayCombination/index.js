const { createDisplayCombination } = require('./createCombination')
const { getProductDisplay } = require('./getCombination')
const { updateDisplayCombination } = require('./updateCombination')

module.exports = {
  createDisplayCombination,
  getProductDisplay,
  updateDisplayCombination
}
