/* eslint-disable node/no-extraneous-require */
/* globals User */
const R = require('ramda')
const createSearchService = require('../../../utils/createSearchService')
const queryBuilder = require('../../../utils/mongoQueryBuilder')
const ObjectId = require('mongodb').ObjectID

const removeFalsyFromArray = R.filter(R.identity)
const userSearch = createSearchService(User)

const createUserSearchPipeline = ({ organization, type, keyword }) => {
  let $match = {
    inactive: false
  }

  if (type) {
    $match = {
      ...$match,
      type: { $in: type }
    }
  }

  if (organization) {
    $match = {
      ...$match,
      organization: ObjectId(organization)
    }
  }

  if (keyword) {
    $match = {
      ...$match,
      $or: removeFalsyFromArray([
        { fullName: { $regex: queryBuilder.toRegExp(`%${keyword}%`) } },
        {
          emailAddress: { $regex: queryBuilder.toRegExp(`%${keyword}%`) }
        },
        {
          type: { $regex: queryBuilder.toRegExp(`%${keyword}%`) }
        },
        !organization && {
          'orgs.name': { $regex: queryBuilder.toRegExp(`%${keyword}%`) }
        }
      ])
    }
  }

  const lookupOrganization = [
    {
      $lookup: {
        from: 'organization',
        let: { orgId: '$organization' },
        pipeline: [
          { $match: { $expr: { $eq: ['$_id', '$$orgId'] } } },
          {
            $addFields: {
              orderName: {
                $toLower: '$name'
              }
            }
          }
        ],
        as: 'orgs'
      }
    }
  ]

  return [...lookupOrganization, { $match }]
}

module.exports = {
  userSearch,
  createUserSearchPipeline,
  createUserSearchOrderBy: queryBuilder.buildOrderBy
}
