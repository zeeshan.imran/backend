const _ = require('@sailshq/lodash')

const toRegExp = value =>
  new RegExp(
    '^' +
      _.escapeRegExp(value)
        .replace(/^%/, '.*')
        .replace(/([^\\])%/g, '$1.*')
        .replace(/\\%/g, '%') +
      '$',
    'gi'
  )

module.exports = toRegExp
