/* globals sails, QrCode, Organization, sails */
const ApiError = require('../../utils/ApiError')
var qr = require('qr-image')
const s3Utils = require('../../utils/s3Utils')
const requestUtils = require('../../utils/requestUtils')
const stream = require('stream')
const {
  qrCodeSearch,
  createQrCodeSearchPipeline
} = require('../services/searches')

const getRedirectLink = (ownerUniqueName, uniqueName) =>
  `${process.env.APP_URL}/qr-code/${ownerUniqueName}/${uniqueName}`

function generateQrCode (ownerUniqueName, uniqueName) {
  if (!process.env.APP_URL) {
    throw new ApiError('NO_APP_URL')
  }

  const redirectLink = getRedirectLink(ownerUniqueName, uniqueName)
  const qrCodePhotoKey = `qr-code/${ownerUniqueName}/${uniqueName}.png`

  // create QR code
  const writable = new stream.PassThrough()
  qr.image(redirectLink, { type: 'png' }).pipe(writable)

  // upload
  return s3Utils.uploadFile(qrCodePhotoKey, writable)
}

module.exports = {
  download: async (req, res) => {
    const id = req.params.id
    const format = req.query.format
    const qrCode = await QrCode.findOne({ id })
    const owner = await Organization.findOne({ id: qrCode.owner })

    const redirectLink = getRedirectLink(owner.uniqueName, qrCode.uniqueName)
    qr.image(redirectLink, { type: format }).pipe(res)
  },
  find: async (req, res) => {
    const owner = req.token.organization

    const searchQuery = {
      ...requestUtils.parseSearchQuery(req),
      owner
    }
    const pipeline = createQrCodeSearchPipeline(searchQuery)
    const { skip, limit } = searchQuery

    const qrCodes = await qrCodeSearch.search(pipeline, {
      skip,
      limit,
      orderBy: { updatedAt: -1 }
    })

    res.ok(qrCodes)
  },
  count: async (req, res) => {
    const owner = req.token.organization

    const searchQuery = {
      ...requestUtils.parseSearchQuery(req),
      owner
    }
    const pipeline = createQrCodeSearchPipeline(searchQuery)

    const rs = await qrCodeSearch.count(pipeline)

    res.ok(rs)
  },
  findOne: async (req, res) => {
    const id = req.params.id
    const qrCode = await QrCode.findOne({ id })

    res.ok(qrCode)
  },
  destroy: async (req, res) => {
    try {
      const id = req.params.id
      const qrCode = await QrCode.findOne({ id })
      if (qrCode.owner !== req.token.organization) {
        throw new ApiError('UNAUTHORIZED')
      }

      const qrCodePhotoKey = s3Utils.getObjectKey(qrCode.qrCodePhoto)
      if (qrCodePhotoKey) {
        try {
          await s3Utils.deleteFile(qrCodePhotoKey)
          sails.log(`${qrCodePhotoKey} was removed`)
        } catch (ex) {
          /* istanbul ignore next */
          sails.log(ex.stack)
        }
      }

      await QrCode.destroy({ id })
      res.ok()
    } catch (e) {
      res.badRequest(e)
    }
  },
  create: async (req, res) => {
    try {
      const creator = req.token.user.id
      const ownerId = req.token.organization
      const { name, uniqueName, targetType, survey, targetLink } = req.body

      const countUniqueName = await QrCode.count({ owner: ownerId, uniqueName })

      if (countUniqueName > 0) {
        throw new ApiError('E_QR_CODE_UNIQUE')
      }

      const owner = await Organization.findOne({ id: ownerId })

      const qrCodePhoto = await generateQrCode(owner.uniqueName, uniqueName)

      res.send(
        await QrCode.create({
          owner: ownerId,
          name,
          uniqueName,
          targetType,
          survey,
          targetLink,
          qrCodePhoto,
          creator
        }).fetch()
      )
    } catch (error) {
      return res.badRequest(error)
    }
  },
  update: async (req, res) => {
    try {
      const id = req.params.id
      const qrCode = await QrCode.findOne({ id })
      const ownerId = qrCode.owner

      // validate owner
      if (ownerId !== req.token.organization) {
        throw new ApiError('UNAUTHORIZED')
      }

      const { name, uniqueName, targetType, survey, targetLink } = req.body

      // validate uniqueName: find the different qrcode with same uniqueName and owner
      const countUniqueName = await QrCode.count({
        uniqueName,
        owner: ownerId,
        id: { '!=': id }
      })

      if (countUniqueName > 0) {
        throw new ApiError('E_QR_CODE_UNIQUE')
      }

      // upload the QR Code
      const owner = await Organization.findOne({ id: ownerId })
      const shouldUpdateQrCode = uniqueName !== qrCode.uniqueName

      const updateSet = {
        name,
        uniqueName,
        targetType,
        survey,
        targetLink,
        modifier: req.token.user.id
      }

      if (shouldUpdateQrCode) {
        updateSet.qrCodePhoto = await generateQrCode(
          owner.uniqueName,
          uniqueName
        )

        // clean up unused photo
        const lastQrCodePhotoKey = s3Utils.getObjectKey(qrCode.qrCodePhoto)
        if (lastQrCodePhotoKey) {
          try {
            // try to delete the last photo
            await s3Utils.deleteFile(lastQrCodePhotoKey)
            sails.log(`${lastQrCodePhotoKey} was removed`)
          } catch (ex) {
            /* istanbul ignore next */
            sails.log(ex.stack)
          }
        }
      }

      res.send(await QrCode.updateOne({ id }).set(updateSet))
    } catch (error) {
      return res.badRequest(error)
    }
  },
  qrCodeByOrganization: async (req, res) => {
    try {
      if (!req.token) {
        return res.unauthorized()
      }
      const { identifier } = req.params
      const { user } = req.token
      const qrCode = await QrCode.findOne({ id: identifier })

      if (qrCode && qrCode.owner === user.organization) {
        return res.ok(qrCode)
      }
      return res.badRequest(
        `Could not find qrcode with the name or id ${identifier}`
      )
    } catch (error) {
      return res.badRequest(error)
    }
  },
  getTargetLink: async (req, res) => {
    const {
      organization: organizationUniqueName,
      qrCode: uniqueName
    } = req.query

    const organization = await Organization.findOne({
      uniqueName: organizationUniqueName
    })

    const qrCode = await QrCode.findOne({ owner: organization.id, uniqueName })

    if (qrCode.targetType === 'survey') {
      return res.ok({
        id: qrCode.id,
        type: 'survey',
        url: `/survey/${qrCode.survey}`
      })
    }

    res.ok({
      id: qrCode.id,
      type: 'link',
      url: qrCode.targetLink
    })
  }
}
