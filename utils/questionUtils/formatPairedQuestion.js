const R = require('ramda')

const factorial = number => {
  if (number === 0) return 1
  return R.range(1, number + 1).reduce((prev, acc) => prev * acc)
}

const combinations = objects => {
  if (objects === 0) return 0
  if (objects === 1) return 1
  return factorial(objects) / (2 * factorial(objects - 2))
}

const formatPairedQuestion = originalQuestion => {
  let pairsLabels = originalQuestion.pairs || []
  const maximumAllowedMinPairs = combinations(originalQuestion.pairs.length)

  let pairsOptions = Object.assign({}, originalQuestion.pairsOptions)
  pairsOptions.minPairs = Math.min(
    maximumAllowedMinPairs,
    pairsOptions.minPairs
  )
  pairsOptions.elementsInPairs = pairsLabels // attach pairs labels to pairsOption

  if (pairsOptions.hasFollowUpProfile) {
    pairsOptions = R.omit(
      ['profileQuestion', 'hasFollowUpProfile'],
      pairsOptions
    )
  }

  let question = Object.assign({}, originalQuestion, { pairsOptions })
  question = R.omit(['pairs'], question)

  return [question, pairsLabels]
}

module.exports = formatPairedQuestion
