/* globals Product, Brand */
const s3Utils = require('../../utils/s3Utils')

module.exports = async product => {
  const {
    name,
    photo,
    brand: brandName,
    reward,
    isAvailable,
    isSurveyCover,
    sortingOrderId
  } = product

  const brand = await Brand.findOrCreate(
    { name: brandName },
    { name: brandName }
  )

  // NOTE: STAGFW-455 tested
  const createdProduct = await Product.create({
    name,
    photo,
    brand: brand.id,
    reward,
    isAvailable,
    isSurveyCover: Boolean(isSurveyCover),
    sortingOrderId
  }).fetch()

  const s3Key = s3Utils.getObjectKey(photo)
  if (s3Key) {
    await s3Utils.resizePhoto(s3Key)
  }

  return createdProduct
}
