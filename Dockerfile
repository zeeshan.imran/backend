FROM node:10.16.0-alpine

ENV TZ=Europe/Zurich
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# RUN npm install -g sails

WORKDIR /api

COPY package.json ./

RUN apk add --update musl-dev &&  rm -rf package-lock.json \ 
  && rm -rf node_modules && npm install

COPY . .

EXPOSE 1337

CMD npm start
