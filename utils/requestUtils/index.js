/**
 * Tries to parse QUERY_STRING[get] to object.
 * Returns empty object ({}) as failback
 * @param {*} req
 */
const parseSearchQuery = req => {
  let { query: rawQuery } = req.allParams()
  return JSON.parse(rawQuery || '{}')
}

module.exports = {
  parseSearchQuery
}
