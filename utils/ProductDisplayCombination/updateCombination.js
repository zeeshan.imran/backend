/* globals ProductDisplay */
/**
 * CreateCombination
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  updateDisplayCombination: async (
    productIds,
    productDisplayType,
    surveyId
  ) => {
    try {
      if (productDisplayType === 'none') {
        const findProductList = await ProductDisplay.findOne({
          survey: surveyId
        })
        if (findProductList && findProductList.id) {
          await ProductDisplay.updateOne({
            survey: surveyId
          }).set({
            products: productIds
          })
        }
      }
      return true
    } catch (error) {
      return error
    }
  }
}
