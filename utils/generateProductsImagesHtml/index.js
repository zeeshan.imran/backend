const generateProductsImagesHtml = imageLinks =>
  imageLinks.reduce((acc, curr) => {
    return acc.concat(`<div
    style="float: left; height: auto; width: calc(50% - 20px); border: 1px solid #e9e9e9; box-sizing: border-box; padding: 19px; margin: 0 10px; margin-bottom: 20px;"
  >
    <img
      style="width: 100%; height: auto;"
      src="${curr}"
    />
  </div>`)
  }, ``)

module.exports = generateProductsImagesHtml
