const defaultCurrencies = {
  'United States of America': 'USD',
  'United Kingdom': 'GBP',
  'Germany': 'EUR'
}

module.exports = defaultCurrencies
