const NEVER_REQUIRED_QUESTIONS_TYPES = new Set(['info', 'profile'])
const NO_NEXT_QUESTION = null // sails ignores "undefined" in requests, so we have to force the change to null
const EMAIL_TEMPLATES = {
  'en': {
    sendSurveyWaitingMail: 'sendSurveyWaitingMail',
    sendSurveyCompletionMail: 'sendSurveyCompletionMail',
    sendSurveyRejectionMail: 'sendSurveyRejectionMail'
  },
  'fr': {
    sendSurveyWaitingMail: 'frSendSurveyWaitingMail',
    sendSurveyCompletionMail: 'frSendSurveyCompletionMail',
    sendSurveyRejectionMail: 'frSendSurveyRejectionMail'
  }
}

module.exports = {
  NEVER_REQUIRED_QUESTIONS_TYPES,
  NO_NEXT_QUESTION,
  EMAIL_TEMPLATES
}
