
var path = require('path')
var fs = require('fs');
var async = require('async');
const findAndRemoveOldFiles =(inputDir, keepCount, callback)=>{
	if(!callback) {
		callback = function (err, removeFiles) {
	            // default callback: doing nothing
	        };
	    };
	    console.log("inputDir",inputDir);
	    fs.readdir(inputDir, function (err, files) {
	    	if(err) {
	    		return callback(err);
	    	}

	    	fileNames = files.map(function (fileName) {
	    		return path.join(inputDir, fileName);   
	    	});
	    	console.log("inputDir",fileNames);
	    	async.map(fileNames, function (fileName, cb) {
	    		fs.stat(fileName, function (err, stat) {
	    			if(err) {
	    				return cb(err);
	    			};

	    			cb(null, {
	    				name: fileName,
	    				isFile: stat.isFile(),
	    				time: stat.mtime,
	    			});
	    		});
	    	}, function (err, files) {

	    		if(err) {
	    			return callback(err);
	    		};

	    		files = files.filter(function (file) {
	    			return file.isFile;
	    		})

	    		files.sort(function (filea, fileb) {
	    			return filea.time < fileb.time;
	    		});

	    		files = files.slice(keepCount);

	    		async.map(files, function (file, cb) {
	    			fs.unlink(file.name, function (err) {
	    				if(err) {
	    					return cb(err);
	    				};

	    				cb(null, file.name);
	    			});
	    		}, function (err, removedFiles) {
	    			if(err) {
	    				return callback(err);
	    			}
	    			callback(null, removedFiles);
	    		});
	    	});
	    });
	}

	const watchAndRemoveOldFiles = (inputDir, keepCount, callback)=>{
		findAndRemoveOldFiles(inputDir, keepCount, callback);
		fs.watch(inputDir, function () {
			findAndRemoveOldFiles(inputDir, keepCount, callback);
		});
	}

	module.exports = {
		watchAndRemoveOldFiles
	}
