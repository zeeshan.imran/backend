/* globals sails */
/* eslint-disable no-console */

const error = sails.log.error

module.exports = { error }
