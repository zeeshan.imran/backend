/* globals sails, Survey, Answer, Question, Answer, User */
/**
 * AnswerController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const R = require('ramda')
const ObjectId = require('mongodb').ObjectID
const _ = require('@sailshq/lodash')

module.exports = {
  getSurveyPhotos: async (req, res) => {
    try {
      const { query } = req.query
      const params = JSON.parse(query)
      const { surveyId, filterBy, currentPage, perPage } = params

      if (!surveyId) return res.badRequest('Not Survey Id Provided')

      const survey = await Survey.findOne({ id: surveyId })

      if (!survey) return res.badRequest('Not Survey Found')

      const questions = await Question.find({
        survey: surveyId,
        typeOfQuestion: 'upload-picture'
      })

      const questionIds = R.map(q => {
        return {
          question: q.id
        }
      })(questions)

      if (questionIds.length) {
        const skipValue = (currentPage - 1) * perPage

        let filterClause = { where: { or: questionIds } }
        switch (filterBy) {
          case 'valid':
          case 'invalid':
            filterClause = { where: { or: questionIds, state: filterBy } }
            break
          case 'not_processed':
            filterClause = { where: { or: questionIds, processed: false } }
            break
        }
        const total = await Answer.count(filterClause)
        const answers = total
          ? await Answer.find(filterClause)
              .skip(skipValue)
              .limit(perPage)
              .populateAll()
          : []

        return res.ok({
          total,
          answers
        })
      }

      return res.ok({
        total: 0,
        answers: []
      })
    } catch (error) {
      return res.badRequest(error)
    }
  },

  updateSurveyPhoto: async (req, res) => {
    try {
      const { user: currentUserToken } = req.token
      const currentUser = await User.findOne({ id: currentUserToken.id })

      if (!currentUser.isSuperAdmin) {
        throw new Error('Not alowed')
      }

      const params = req.body
      const updated = await Answer.updateOne({ id: params.id }).set({
        comment: params.comment,
        state: params.state,
        processed: params.processed
      })
      return res.ok(updated)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  getSurveyAnswers: async (req, res) => {
    const { enrollmentId } = req.params
    const aggregateArray = [
      { $match: { enrollment: ObjectId(enrollmentId) } },
      {
        $lookup: {
          from: 'question',
          localField: 'question',
          foreignField: '_id',
          as: 'question'
        }
      },
      {
        $lookup: {
          from: 'product',
          localField: 'product',
          foreignField: '_id',
          as: 'product'
        }
      },
      { $unwind: '$question' },
      { $unwind: { path: '$product', preserveNullAndEmptyArrays: true } },
      {
        $project: {
          _id: 1,
          prompt: '$question.prompt',
          value: 1,
          product: '$product.name',
          typeOf: '$question.typeOfQuestion',
          verticalRange: '$question.range',
          numericOptions: '$question.numericOptions',
          option: '$question.options',
          chartTopic: '$question.chartTopic',
          chartTitle: '$question.chartTitle',
          pairsOptions: '$question.pairsOptions',
          question_id: '$question._id'
        }
      }
    ]
    const db = Answer.getDatastore().manager
    const cursor = await db
      .collection(Answer.tableName)
      .aggregate(aggregateArray)
    const docs = await cursor.toArray()

    const newDocs = docs
      .filter(function (el) {
        return (
          el.typeOf !== 'upload-picture' &&
          el.typeOf !== 'time-stamp' &&
          el.typeOf !== 'paired-questions'
        )
      })
      .map(item => {
        switch (item.typeOf) {
          case 'numeric':
            return { ...item, ...{ mainValue: item.value[0] } }
          case 'dropdown':
          case 'location':
            const found = item.option.find(
              element => element.value === item.value[0]
            )
            return { ...item, ...{ mainValue: found.label } }
          case 'choose-one':
          case 'select-and-justify':
            let chooseOneSelection = null
            if (typeof item.value[0] === 'object') {
              chooseOneSelection = item.value[0].desc
              return { ...item, ...{ mainValue: chooseOneSelection } }
            } else {
              chooseOneSelection = item.option.find(
                element => element.value === item.value[0]
              )
              return { ...item, ...{ mainValue: chooseOneSelection.label } }
            }

          case 'choose-multiple':
            const { value } = item
            const mainValue = []
            value.forEach(val => {
              const found = item.option.find(element => element.value === val)
              if (found && found.label) {
                mainValue.push(found.label)
              } else if (val && val.value === 'other') {
                mainValue.push(val.desc)
              }
            })
            return { ...item, ...{ mainValue } }
          case 'vertical-rating':
            const range = parseInt(item.value[0]) - 1
            if (item.verticalRange.labels[range]) {
              return {
                ...item,
                ...{ mainValue: item.verticalRange.labels[range] }
              }
            } else {
              return { ...item, ...{ mainValue: item.value[0] } }
            }
          case 'slider':
            return { ...item, ...{ mainValue: item.value[0].label } }
          default:
            return { ...item, ...{ mainValue: item.value[0] } }
        }
      })

    const pairedQuestion = docs.filter(function (el) {
      return el.typeOf === 'paired-questions'
    })
    let uniquePairedValues = []
    if (pairedQuestion.length > 0) {
      uniquePairedValues = _.uniq(pairedQuestion, 'prompt')
    }
    const finalResult = [...newDocs, ...uniquePairedValues]
    return res.ok(finalResult)
  }
}
