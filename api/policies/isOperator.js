module.exports = function (req, res, next) {
  if (!req.token) {
    return res.forbidden('Not authorized')
  }
  const { user } = req.token

  if (user.type === 'operator') {
    return next()
  } else {
    res.forbidden('Not authorized')
  }
}
