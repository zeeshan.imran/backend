/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your actions.
 *
 * For more information on configuring policies, check out:
 * https://sailsjs.com/docs/concepts/policies
 */

module.exports.policies = {
  '*': false,

  ProductController: {
    find: true,
    findOne: true,
    replaceProducts: ['authToken', 'isOperator'],
    create: ['authToken', 'isOperator'],
    update: ['authToken', 'isOperator']
  },

  BrandController: {
    find: true,
    findOne: true
  },

  OrganizationController: {
    find: ['authToken', 'isOperator'],
    organization: ['authToken'],
    findOne: ['authToken', 'isOperator'],
    organizations: ['authToken', 'is-super-admin'],
    countOrganizations: ['authToken', 'is-super-admin'],
    addOrganization: ['authToken', 'is-super-admin'],
    editOrganization: ['authToken', 'is-super-admin'],
    deleteOrganization: ['authToken', 'is-super-admin']
  },

  SurveyController: {
    find: true,
    getScreenerSurveys: true,
    getCoversOfSurveys: true,
    update: ['authToken', 'isOperator'],
    createSurvey: ['authToken', 'isOperator'],
    updateSurvey: ['authToken', 'isOperator'],
    findOne: true,
    dispatchEmail: true,
    getDemoSurvey: true,
    getSurvey: true,
    getSurveyByOrganization: ['authToken'],
    create: ['authToken', 'isOperator'],
    cloneSurvey: ['authToken', 'isOperator'],
    deprecateSurvey: ['authToken', 'isOperator'],
    getSurveysByOwner: ['authToken', 'isOperatorOrAnalytics'],
    countSurveys: ['authToken', 'isOperatorOrAnalytics'],
    isUniqueNameDuplicated: ['authToken', 'isOperator'],
    getSurveyShareStats: ['authToken'],
    getGiftCardShares: ['authToken'],
    getProductIncentiveStats: ['authToken'],
    getGiftCardIncentives: ['authToken'],
    shareSurveyStats: ['authToken', 'isSuperAdminOrPowerUser'],
    getSurveyPdf: ['authToken'],
    getTasterPdf: true,
    downloadPdf: true
  },

  SystemController: {
    resetShareLinks: ['authToken', 'is-super-admin']
  },

  QuestionController: {
    findOne: true,
    replaceQuestions: ['authToken', 'isOperator'],
    create: ['authToken', 'isOperator'],
    findIds: ['authToken', 'isOperator'],
    update: ['authToken', 'isOperator'],
    delete: ['authToken', 'isOperator']
  },

  PairQuestionController: {
    create: ['authToken', 'isOperator'],
    generatePairs: ['authToken', 'isOperator'],
    updatePairs: ['authToken', 'isOperator']
  },

  UserController: {
    loginToSurvey: true,
    loginToSurveyWithAuth: ['authToken'],
    loginUser: true,
    find: ['authToken'],
    findOne: ['authToken'],
    update: ['authToken', 'isLoggedIn'],
    updateUserPassword: ['authToken', 'isLoggedIn'],
    dispatchRequestAccountEmail: true,
    forgotPassword: true,
    resetPassword: true,
    me: ['authToken'],
    tasterDashboard: ['authToken'],
    updateUser: ['authToken'],
    users: ['authToken', 'isSuperAdminOrPowerUser'],
    addUser: ['authToken', 'isSuperAdminOrPowerUser'],
    editUser: ['authToken', 'isSuperAdminOrPowerUser'],
    deleteUser: ['authToken', 'isSuperAdminOrPowerUser'],
    countUsers: ['authToken', 'isSuperAdminOrPowerUser'],

    createTasterAccount: true,
    tasterResendVerificationEmail: true,
    updateTasterAccount: ['authToken'],
    isRegistered: true,
    verifyTaster: true
  },

  SurveyEnrollmentController: {
    saveRewards: true,
    setSurveySelectedProducts: true,
    find: true,
    findOne: true,
    setPaypalEmail: true,
    submitAnswer: true,
    submitForcedSignUp: true,
    setState: true,
    finishSurvey: true,
    finishSurveyScreening: true,
    getSurveyEnrollementValidation: ['authToken', 'isSuperAdminOrPowerUser'],
    updateSurveyEnrollementValidation: ['authToken', 'isSuperAdminOrPowerUser'],
    updateSurveyEnrollementProductIncentives: [
      'authToken',
      'isSuperAdminOrPowerUser'
    ],
    updateSurveyShareStatus: ['authToken', 'isSuperAdminOrPowerUser'],
    rejectSurvey: true,
    getSurveyFunnel: ['authToken', 'isSuperAdminOrPowerUser'],
    sendSurveyStats: ['authToken', 'isOperator'],
    updateSurveyEnrollementGiftCardIncentives: [
      'authToken',
      'isSuperAdminOrPowerUser'
    ]
  },

  QrCodeController: {
    findOne: ['authToken', 'isOperator'],
    download: ['authToken', 'isOperator', 'isBelongToOrganization'],
    find: ['authToken', 'isOperator', 'isBelongToOrganization'],
    count: ['authToken', 'isOperator', 'isBelongToOrganization'],
    create: ['authToken', 'isOperator', 'isBelongToOrganization'],
    update: ['authToken', 'isOperator', 'isBelongToOrganization'],
    destroy: ['authToken', 'isOperator', 'isBelongToOrganization'],
    qrCodeByOrganization: ['authToken'],
    getTargetLink: true
  },

  ImageController: {
    addImage: ['authToken', 'isOperator']
  },

  SettingsController: {
    saveChartsSettings: ['authToken'],
    getChartsSettings: true
  },

  AnswerController: {
    getSurveyAnswers: true
  }
}
