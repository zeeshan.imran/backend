const languages = ['en', 'de', 'kr', 'fr', 'pt', 'es']
const resources = languages.reduce((resources, lang) => {
  try {
    return {
      ...resources,
      [lang]: { translation: require(`./${lang}.json`) }
    }
  } catch (ex) {
    //
  }
}, {})

module.exports = { resources, languages }
