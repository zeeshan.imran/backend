const { generatePairs, cleanPairs } = require('../../utils/questionUtils')
/**
 * PairQuestionController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  generatePairs: async (req, res) => {
    try {
      const { pairs, questionId } = req.body
      await generatePairs(pairs, questionId)
      return res.ok()
    } catch (error) {
      return res.badRequest(error)
    }
  },
  updatePairs: async (req, res) => {
    try {
      const { pairs, questionId } = req.body
      // destroy existing pairs
      await cleanPairs(questionId)
      await generatePairs(pairs, questionId)
      return res.ok()
    } catch (error) {
      return res.badRequest(error)
    }
  }
}
