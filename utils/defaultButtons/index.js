const defaultButtons = {
  continue: 'continue',
  start: 'start',
  next: 'next',
  skip: 'skip'
}

module.exports = defaultButtons
