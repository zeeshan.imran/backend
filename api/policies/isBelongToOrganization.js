const ApiError = require('../../utils/ApiError')

module.exports = function (req, res, next) {
  if (!req.token) {
    return res.forbidden('Not authorized')
  }
  const { user } = req.token

  if (user.type === 'operator') {
    if (!user.organization) {
      return res.badRequest(new ApiError('NO_ORGANIZATION'))
    }

    req.token.organization = user.organization
    return next()
  } else {
    res.forbidden('Not authorized')
  }
}
