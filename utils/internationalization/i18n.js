const i18n = require('i18next')
const { resources } = require('./index')
const Backend = require('i18next-node-fs-backend')

i18n.use(Backend).init({
  resources: resources,
  // the language to use
  lng: 'en',
  // if the key is not found in lng, then the key is searched in fallback
  fallbackLng: 'en',

  // have a common namespace used around the full app
  ns: ['translation'],
  defaultNS: 'translation',

  // the separator to be used in t(category.key2)
  keySeparator: '.',

  interpolation: {
    escapeValue: false
  }
})

module.exports = i18n
