const PERMANENT_ACCOUNT_TYPES = new Set(['operator', 'analytics', 'taster', 'power-user'])

const isPermanentAccount = userType => PERMANENT_ACCOUNT_TYPES.has(userType)

module.exports = isPermanentAccount
