/* globals Survey */
/**
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const ObjectId = require("mongodb").ObjectID

const getScreenerSurveys = async surveyId => {
  const db = Survey.getDatastore().manager
  const cursor = await db
    .collection('survey_linkedSurveys__survey_linkedSurveys_survey')
    .aggregate([
      {
        $match: {
          survey_linkedSurveys_survey: ObjectId(surveyId)
        }
      },
      {
        $lookup: {
          from: 'survey',
          localField: 'survey_linkedSurveys',
          foreignField: '_id',
          as: 'screenerSurveys'
        }
      },
      { $unwind: '$screenerSurveys' },
      { $replaceRoot: { newRoot: '$screenerSurveys' } }
    ])

  return cursor.toArray()
}

module.exports = {
  getScreenerSurveys: getScreenerSurveys
}
