/* globals User, SurveyEnrollment, Survey, Organization, jwToken, process, emailService, Answer _ */
/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const bcrypt = require('bcryptjs')
const isPermanentAccount = require('../../utils/isPermanentAccount')
const toLower = require('../../utils/toLower')
const requestUtils = require('../../utils/requestUtils')
const ApiError = require('../../utils/ApiError')
const moment = require('moment')
const R = require('ramda')

const {
  userSearch,
  createUserSearchPipeline,
  createUserSearchOrderBy
} = require('../services/searches')

const { getProductDisplay } = require('../../utils/ProductDisplayCombination')

const updateUserRecordIfDeleted = async (userData, currentUser, req, res) => {
  if (userData) {
    let { emailAddress } = userData
    emailAddress = toLower(emailAddress)
    const prevProfile = await User.findOne({ emailAddress: emailAddress })
    if (prevProfile && prevProfile.inactive) {
      userData['inactive'] = false

      if (currentUser.isSuperAdmin) {
        userData['organization'] = req.body.organization
      } else if (currentUser.type === 'power-user') {
        userData['organization'] = currentUser.organization
      }

      const newUser = await User.updateOne({ id: prevProfile.id }).set(userData)
      return res.ok(newUser)
    }
  }
  throw new ApiError('E_UNIQUE_USER')
}

const logUserToSurvey = async (req, res) => {
  const {
    email: inputEmail,
    survey: surveyId,
    referral,
    password,
    country,
    browserInfo
  } = req.body
  const email = toLower(inputEmail)

  let surveyEnrollment
  let referralUser
  let isReferredUser = false

  const populatedSurvey = await Survey.findOne(
    { id: surveyId },
    { questions: true, exclusiveTasters: true }
  )

  if (!populatedSurvey) {
    return res.badRequest('No survey found for the given id')
  }

  if (referral) {
    referralUser = await User.findOne({ id: referral })
    isReferredUser = true
  }

  if (referralUser && referralUser.inactive) {
    return res.badRequest('Not a valid account')
  }

  if (referralUser && referralUser.emailAddress === email) {
    return res.badRequest(`Users can't refer themselves`)
  }

  if (populatedSurvey.authorizationType === 'selected') {
    if (!email) {
      return res.badRequest(`Need an e-mail to login`)
    }

    let foundTaster = populatedSurvey.exclusiveTasters.find(
      taster => taster.emailAddress === email
    )

    if (
      populatedSurvey.state === 'draft' &&
      req.token &&
      req.token.user.organization === populatedSurvey.owner
    ) {
      foundTaster = req.token.user
      populatedSurvey.allowRetakes = true
    }

    if (!foundTaster) {
      return res.forbidden("This taster isn't allowed to participate")
    }

    if (!populatedSurvey.allowRetakes) {
      const hasTakenPreviously = await SurveyEnrollment.find({
        survey: surveyId,
        user: foundTaster.id,
        state: ['finished', 'rejected']
      })
      if (hasTakenPreviously.length) {
        return res.badRequest(
          `Retakes not allowed - ${hasTakenPreviously[0]['state']}`
        )
      }
    }

    surveyEnrollment = await SurveyEnrollment.findOne({
      user: foundTaster.id,
      survey: surveyId,
      state: { nin: ['finished', 'rejected'] }
    })
      .populate('lastAnsweredQuestion')
      .populate('survey')
      .populate('user')
      .populate('selectedProducts')
    if (!surveyEnrollment) {
      const {
        productDisplay,
        productDisplayOrder,
        productDisplayType
      } = await getProductDisplay(surveyId)
      surveyEnrollment = await SurveyEnrollment.create({
        user: foundTaster.id,
        survey: surveyId,
        lastAnsweredQuestion: null,
        referral: referral,
        savedRewards: [],
        browserInfo: browserInfo || [],
        productDisplay: productDisplay,
        productDisplayOrder: productDisplayOrder,
        productDisplayType: productDisplayType
      }).fetch()
    }

    const answers = await Answer.find({
      enrollment: surveyEnrollment.id
    })

    return res.ok({
      ...surveyEnrollment,
      answers
    })
  }

  if (!email) {
    const {
      productDisplay,
      productDisplayOrder,
      productDisplayType
    } = await getProductDisplay(surveyId)
    surveyEnrollment = await SurveyEnrollment.create({
      survey: surveyId,
      lastAnsweredQuestion: null,
      referral: referral,
      browserInfo: browserInfo || [],
      productDisplay: productDisplay,
      productDisplayOrder: productDisplayOrder,
      productDisplayType: productDisplayType
    }).fetch()
    return res.ok(surveyEnrollment)
  }

  let user = await User.findOne({ emailAddress: email })

  if (!user) {
    user = await User.create({
      emailAddress: email,
      password,
      country,
      type: 'taster',
      isTaster: true,
      isReferredUser
    }).fetch()
    if (user && user.password && user.password !== '') {
      await emailService.sendWelcomeEmail({
        email,
        language: user.language
      })
    }
  } else {
    if (!user.password || user.password === '') {
      const updatedUser = await User.updateOne({ id: user.id }).set({
        password,
        country,
        isReferredUser,
        type: user.type === 'respondent' ? `taster` : user.type
      })
      if (updatedUser && updatedUser.password && updatedUser.password !== '') {
        await emailService.sendWelcomeEmail({
          email,
          language: user.language
        })
      }
    }
    if (!populatedSurvey.allowRetakes) {
      const hasTakenPreviously = await SurveyEnrollment.find({
        survey: surveyId,
        user: user.id,
        state: ['finished', 'rejected']
      })
      if (hasTakenPreviously.length) {
        return res.badRequest(
          `Retakes not allowed - ${hasTakenPreviously[0]['state']}`
        )
      }
    }
  }

  surveyEnrollment = await SurveyEnrollment.findOne({
    user: user.id,
    survey: surveyId,
    state: { nin: ['finished', 'rejected'] }
  })
    .populate('lastAnsweredQuestion')
    .populate('survey')
    .populate('user')
    .populate('selectedProducts')

  if (!surveyEnrollment) {
    const {
      productDisplay,
      productDisplayOrder,
      productDisplayType
    } = await getProductDisplay(surveyId)
    surveyEnrollment = await SurveyEnrollment.create({
      user: user.id,
      survey: surveyId,
      lastAnsweredQuestion: null,
      referral: referral,
      savedRewards: [],
      browserInfo: browserInfo || [],
      productDisplay: productDisplay,
      productDisplayOrder: productDisplayOrder,
      productDisplayType: productDisplayType
    }).fetch()
  }

  if (populatedSurvey.authorizationType === 'email') {
    const expired =
      moment(surveyEnrollment.createdAt).add(
        populatedSurvey.allowedDaysToFillTheTasting,
        'days'
      ) < moment()
    if (expired) {
      return res.badRequest('Survey expired')
    }
  }

  const answers = await Answer.find({
    enrollment: surveyEnrollment.id
  })

  return res.ok({
    ...surveyEnrollment,
    answers
  })
}

module.exports = {
  me: async (req, res) => {
    try {
      const { user } = req.token

      return res.ok(user)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  tasterDashboard: async (req, res) => {
    /* Logic implemented
      1 - Survey has to be active
      2 - Survey's country has to match the users country
      3 - User will see all the screenerOnly surveys from its country only
      4 - User will see the linkedSurvey only if they complete and qualify from screenerOnly survey
      5 - User can see all the surveys he's been selected
    */
    try {
      let { user: currentUserToken } = req.token
      const user = await User.findOne({
        id: currentUserToken.id
      }).populate('exclusiveSurveys')

      const exclusivIds = user.exclusiveSurveys.map((exclusiveSurvey) => { return exclusiveSurvey.id })
      user.surveyEnrollments = await SurveyEnrollment.find({
        user: user.id
      }).sort([
        { createdAt: 'DESC' }
      ])
      const screenerSurveys = await Survey.find({ isScreenerOnly: true, owner: currentUserToken.organization, state: 'active' }).populate('linkedSurveys')
      let linkedSurveyIds = []
      screenerSurveys.forEach((survey) => {
        if (survey.linkedSurveys) {
          return survey.linkedSurveys.forEach((linkedSurvey) => {
            linkedSurveyIds.push(linkedSurvey.id)
          })
        }
      })
      user.exclusiveSurveys = await Survey.find({
        where: {
          or: [
            { id: { in: exclusivIds }, state: 'active', owner: currentUserToken.organization },
            { authorizationType: 'public', state: 'active', owner: currentUserToken.organization, showOnTasterDashboard: true },
            { isScreenerOnly: true, state: 'active', owner: currentUserToken.organization, country: currentUserToken.country, id: { nin: linkedSurveyIds } },
            { state: 'active', country: currentUserToken.country, owner: currentUserToken.organization, id: { nin: linkedSurveyIds } }
          ]
        }
      })
        .populate('products')
        .populate('linkedSurveys')
        .sort([
          { createdAt: 'DESC' }
        ])
      return res.ok(user)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  users: async (req, res) => {
    try {
      let { user: currentUserToken, superUser } = req.token
      if (superUser) {
        currentUserToken = superUser
      }
      const currentUser = await User.findOne({ id: currentUserToken.id })

      let organization
      if (currentUser.isSuperAdmin) {
        organization = undefined
      } else if (currentUser.type === 'power-user') {
        organization = currentUser.organization
      } else {
        throw new Error('Not alowed')
      }

      const searchQuery = {
        ...requestUtils.parseSearchQuery(req),
        organization
      }
      const pipeline = createUserSearchPipeline(searchQuery)
      const { skip, limit } = searchQuery
      const orderBy = createUserSearchOrderBy(searchQuery)

      const sr = await userSearch.search(pipeline, {
        skip,
        limit,
        orderBy: orderBy || false
      })

      return res.ok(sr)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  countUsers: async (req, res) => {
    try {
      let { user: currentUserToken, superUser } = req.token
      if (superUser) {
        currentUserToken = superUser
      }
      const currentUser = await User.findOne({ id: currentUserToken.id })

      let organization
      if (currentUser.isSuperAdmin) {
        organization = undefined
      } else if (currentUser.type === 'power-user') {
        organization = currentUser.organization
      } else {
        throw new Error('Not alowed')
      }

      const searchQuery = {
        ...requestUtils.parseSearchQuery(req),
        organization
      }
      const pipeline = createUserSearchPipeline(searchQuery)

      const sr = await userSearch.count(pipeline)
      return res.ok(sr)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  addUser: async (req, res) => {
    let currentUser
    let newUserData
    try {
      const { user: currentUserToken } = req.token
      currentUser = await User.findOne({ id: currentUserToken.id })
      newUserData = {
        emailAddress: toLower(req.body.emailAddress),
        fullName: req.body.fullName,
        type: req.body.type
      }
      if (currentUser.isSuperAdmin) {
        newUserData = {
          ...newUserData,
          inactive: false,
          organization: req.body.organization,
          isSuperAdmin: req.body.isSuperAdmin,
          isTaster: req.body.isTaster
        }
      } else if (currentUser.type === 'power-user') {
        newUserData = {
          ...newUserData,
          inactive: false,
          organization: currentUser.organization
        }
      } else {
        throw new Error('Not alowed')
      }

      const newUser = await User.create(newUserData).fetch()
      return res.ok(newUser)
    } catch (error) {
      if (error.code === 'E_INVALID_NEW_RECORD') {
        throw new ApiError(error.code)
      }
      if (error.code === 'E_UNIQUE') {
        const updatedRecord = await updateUserRecordIfDeleted(
          newUserData,
          currentUser,
          req,
          res
        )
        return updatedRecord
      } else {
        return res.badRequest(error)
      }
    }
  },
  editUser: async (req, res) => {
    try {
      const { user: currentUserToken } = req.token
      const currentUser = await User.findOne({ id: currentUserToken.id })
      let newUserData = {
        emailAddress: toLower(req.body.emailAddress),
        fullName: req.body.fullName,
        type: req.body.type
      }
      if (currentUser.isSuperAdmin) {
        newUserData = {
          ...newUserData,
          inactive: false,
          organization: req.body.organization,
          isSuperAdmin: req.body.isSuperAdmin,
          isTaster: req.body.isTaster
        }
      } else if (currentUser.type === 'power-user') {
        newUserData = {
          ...newUserData,
          inactive: false,
          organization: currentUser.organization
        }
      } else {
        throw new Error('Not alowed')
      }

      const newUser = await User.updateOne({ id: req.body.id }).set(newUserData)
      return res.ok(newUser)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  updateUser: async (req, res) => {
    try {
      const { user } = req.token
      if (!user) {
        return res.forbidden('Not authorized')
      }

      const currentLoggedInUser = await User.findOne({
        emailAddress: toLower(user.emailAddress)
      })

      const { body } = req
      const { id } = req.params

      if (
        id === currentLoggedInUser.id ||
        currentLoggedInUser.isSuperAdmin ||
        currentLoggedInUser.type === 'power-user'
      ) {
        const userUpdated = await User.updateOne({ id: id }).set(body)
        return res.ok(userUpdated)
      } else {
        return res.forbidden('Not authorized')
      }
    } catch (error) {
      return res.badRequest(error)
    }
  },
  deleteUser: async (req, res) => {
    try {
      const { user: currentUserToken } = req.token
      const currentUser = await User.findOne({ id: currentUserToken.id })
      if (currentUser.isSuperAdmin) {
        await User.updateOne({ id: req.body.id }).set({ inactive: true })
      } else if (currentUser.type === 'power-user') {
        await User.updateOne({
          id: req.body.id,
          organization: currentUser.organization
        }).set({ inactive: true })
      } else {
        throw new Error('Not alowed')
      }
      return res.ok(true)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  loginToSurvey: async (req, res) => {
    try {
      return await logUserToSurvey(req, res)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  loginToSurveyWithAuth: async (req, res) => {
    try {
      return await logUserToSurvey(req, res)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  loginUser: async (req, res) => {
    try {
      const { email: inputEmail, password } = req.body
      const email = toLower(inputEmail)
      const user = await User.findOne({ emailAddress: email })
      if (!user) {
        return res.badRequest('INVALID:Username or password are not correct.')
      }

      if (!isPermanentAccount(user.type) || (user && user.inactive)) {
        return res.badRequest('Not a valid account')
      }

      if (password && bcrypt.compareSync(password, user.password)) {
        const token = jwToken.issue(
          Object.assign(
            {},
            _.pick(user, [
              'id',
              'emailAddress',
              'fullName',
              'organization',
              'type'
            ]),
            {
              isSuperAdmin: user.isSuperAdmin ? true : undefined,
              apiVersion: process.env.USER_API_VERSION
            }
          )
        )
        const response = {
          user: { ...user, isSuperAdmin: user.isSuperAdmin },
          token
        }
        return res.ok(response)
      } else {
        return res.badRequest('INVALID:Username or password are not correct.')
      }
    } catch (error) {
      return res.badRequest(error)
    }
  },
  createTasterAccount: async (req, res) => {
    let { emailAddress, password, type } = req.body
    emailAddress = toLower(emailAddress)
    try {
      let newUserData = {
        emailAddress: emailAddress,
        password: password,
        type: type
      }
      const hasOperatorAccount = await User.findOne({
        emailAddress,
        type: 'operator'
      })
      if (hasOperatorAccount) {
        const superAdmin = await User.find({ isSuperAdmin: true })

        if (superAdmin.length) {
          await emailService.sendOperatorRequestTasterAccountEmail({
            adminEmail: toLower(superAdmin[0].emailAddress),
            emailAddress
          })
        }

        return res.badRequest('Operator Account Exists')
      }
      const org = await Organization.findOne({ uniqueName: 'flavor-wiki' })
      if (!newUserData.organization) {
        newUserData.organization = org.id
      }
      const newUser = await User.create(newUserData).fetch()
      if (newUser) {
        const token = jwToken.issue(
          Object.assign(
            {},
            _.pick(newUser, [
              'id',
              'emailAddress',
              'fullName',
              'organization',
              'type'
            ]),
            {
              isSuperAdmin: newUser.isSuperAdmin ? true : undefined,
              apiVersion: process.env.USER_API_VERSION
            }
          )
        )

        if (newUser.type === 'taster' && newUser.password !== '') {
          const salt = bcrypt.genSaltSync(10)
          const verificationToken = bcrypt.hashSync(emailAddress, salt)

          await User.updateOne({ emailAddress: emailAddress }).set({
            verificationToken: verificationToken,
            verificationTokenExpiresAt: Date.now() + 24 * 60 * 60 * 1000 // now + 1 day in ms
          })

          const verificationLink = `${
            process.env.APP_URL
          }/onboarding/verify-email?token=${verificationToken}&email=${encodeURIComponent(
            emailAddress
          )}`
          await emailService.sendVerificationEmail({
            email: newUser.emailAddress,
            verifyLink: verificationLink,
            language: newUser.language || 'en'
          })
        }

        const response = {
          user: newUser,
          token
        }
        return res.ok(response)
      } else {
        return res.badRequest('INVALID:Username or password are not correct.')
      }
    } catch (error) {
      if (error.code === 'E_UNIQUE') {
        return res.badRequest('Email Address already in use.')
      } else return res.badRequest(error)
    }
  },
  tasterResendVerificationEmail: async (req, res) => {
    try {
      let { emailAddress } = req.body
      if (!emailAddress) return res.badRequest('Email address is mandatory.')
      emailAddress = toLower(emailAddress)
      const userDetail = await User.findOne({
        emailAddress
      })
      if (!userDetail) {
        return res.badRequest('Email address not found.')
      } else if (userDetail.type === 'taster') {
        const salt = bcrypt.genSaltSync(10)
        const verificationToken = bcrypt.hashSync(emailAddress, salt)

        await User.updateOne({ emailAddress: emailAddress }).set({
          verificationToken: verificationToken,
          verificationTokenExpiresAt: Date.now() + 24 * 60 * 60 * 1000 // now + 1 day in ms
        })

        const verificationLink = `${
          process.env.APP_URL
        }/onboarding/verify-email?token=${verificationToken}&email=${encodeURIComponent(
          emailAddress
        )}`
        await emailService.sendVerificationEmail({
          email: userDetail.emailAddress,
          verifyLink: verificationLink,
          language: userDetail.language || 'en'
        })
      }
      const response = {
        user: userDetail
      }
      return res.ok(response)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  updateTasterAccount: async (req, res) => {
    try {
      const { user: currentUserToken } = req.token
      const currentUser = await User.findOne({ id: currentUserToken.id })
      req.body['initialized'] = true
      if (currentUser.id === req.body.id) {
        const newUser = await User.updateOne({ id: req.body.id }).set(req.body)
        return res.ok(newUser)
      } else {
        return res.forbidden('Not authorized')
      }
    } catch (error) {
      return res.badRequest(error.message)
    }
  },
  updateUserPassword: async (req, res) => {
    try {
      const { currentPassword, newPassword } = req.body
      const { user: currentUserToken } = req.token
      const currentUser = await User.findOne({ id: currentUserToken.id })
      if (!currentUser) {
        throw new Error('Not Authorized')
      }
      if (bcrypt.compareSync(currentPassword, currentUser.password)) {
        await User.updateOne({ id: currentUserToken.id }).set({
          password: newPassword
        })
        return res.ok(true)
      } else {
        return res.ok(false)
      }
    } catch (error) {
      return res.badRequest(error)
    }
  },
  dispatchRequestAccountEmail: async (req, res) => {
    try {
      let {
        email,
        userType,
        firstName,
        lastName,
        companyName,
        phoneNumber
      } = req.body
      email = toLower(email)

      const hasTasterAccount = await User.findOne({
        emailAddress: email,
        type: 'taster'
      })
      if (hasTasterAccount) {
        const superAdmin = await User.find({ isSuperAdmin: true })

        if (superAdmin.length) {
          await emailService.sendTasterRequestOperatorAccountEmail({
            adminEmail: superAdmin[0].emailAddress,
            emailAddress: email
          })
        }

        return res.badRequest('Taster Account Exists')
      }

      const user =
        hasTasterAccount ||
        (await User.findOne({ emailAddress: email, inactive: false }))
      if (user) {
        throw new ApiError('E_USER_IS_EXISTED')
      }

      await emailService.sendRequestAccountEmail({
        requesterEmail: email,
        userType,
        firstName,
        lastName,
        companyName,
        phoneNumber
      })
      return res.ok()
    } catch (error) {
      return res.badRequest(error)
    }
  },
  forgotPassword: async (req, res) => {
    try {
      let { email } = req.body
      email = toLower(email)
      const user = await User.findOne({ emailAddress: email })

      if (!user) {
        return res.badRequest('That email is not registered.')
      }

      if (!isPermanentAccount(user.type)) {
        return res.badRequest('Not a valid account')
      }

      const salt = bcrypt.genSaltSync(10)
      const resetToken = bcrypt.hashSync(user.emailAddress, salt)

      await User.updateOne({ emailAddress: email }).set({
        passwordResetToken: resetToken,
        passwordResetTokenExpiresAt: Date.now() + 24 * 60 * 60 * 1000 // now + 1 day in ms
      })

      const resetLink = `${
        process.env.APP_URL
      }/onboarding/reset-password?token=${resetToken}&email=${encodeURIComponent(
        email
      )}`

      await emailService.sendResetPasswordEmail({
        email,
        resetLink,
        language: user.language
      })

      return res.ok()
    } catch (error) {
      return res.badRequest(error)
    }
  },
  resetPassword: async (req, res) => {
    try {
      let { email, password, token } = req.body
      email = toLower(email)
      const user = await User.findOne({ emailAddress: email })

      if (!user) {
        return res.badRequest('That email is not registered.')
      }

      if (!isPermanentAccount(user.type)) {
        return res.badRequest('Not a valid account')
      }

      if (
        token === user.passwordResetToken &&
        Date.now() < user.passwordResetTokenExpiresAt
      ) {
        await User.updateOne({ emailAddress: email }).set({
          password,
          passwordResetToken: '',
          passwordResetTokenExpiresAt: 0
        })
        return res.ok()
      } else {
        return res.forbidden('Not authorized')
      }
    } catch (error) {
      return res.badRequest(error)
    }
  },
  isRegistered: async (req, res) => {
    try {
      let { email } = req.body
      email = toLower(email)
      const user = await User.findOne({ emailAddress: email })

      if (!user) {
        await User.create({ emailAddress: email })
        return res.badRequest()
      }

      if (!user.password || user.password === '') {
        return res.badRequest()
      }

      return res.ok()
    } catch (error) {
      return res.badRequest(error)
    }
  },
  verifyTaster: async (req, res) => {
    try {
      let { emailAddress, token } = req.body
      emailAddress = toLower(emailAddress)
      const user = await User.findOne({ emailAddress: emailAddress })
      if (!user) {
        return res.badRequest('That email is not registered.')
      }

      if (!isPermanentAccount(user.type)) {
        return res.badRequest('Not a valid account')
      }

      if (
        token === user.verificationToken &&
        Date.now() < user.verificationTokenExpiresAt
      ) {
        await User.updateOne({ emailAddress: emailAddress }).set({
          isVerified: true,
          passwordResetTokenExpiresAt: 0,
          isTaster: true
        })
        const token = jwToken.issue(
          Object.assign(
            {},
            _.pick(user, [
              'id',
              'emailAddress',
              'fullName',
              'organization',
              'type'
            ]),
            {
              isTaster: user.isSuperAdmin ? true : undefined,
              apiVersion: process.env.USER_API_VERSION
            }
          )
        )
        const response = {
          user: { ...user },
          token
        }
        return res.ok(response)
      } else {
        return res.forbidden('Not authorized')
      }
    } catch (error) {
      return res.badRequest(error)
    }
  }
}
