const { createId } = require('../utils')
const organizations = require('./organizations')

const surveys = [
  {
    id: createId(),
    uniqueName: 'survey-1',
    name: 'Survey 1',
    owner: organizations[0].id
  }
]
module.exports = surveys
