const imageCreation = require('../../utils/imageCreation')

module.exports = {
  addImage: async (req, res) => {
    try {
      const { image } = req.body
      const imagePath = await imageCreation(image)
      return res.ok(imagePath)
    } catch (error) {
      return res.badRequest(error)
    }
  }
}
