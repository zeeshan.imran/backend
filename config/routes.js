/* globals sails */
/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

const fs = require('fs')
const path = require('path')
const uploadService = require('../api/services/uploadService')

module.exports.routes = {
  'GET /': (req, res) => res.send({ health: 'ok' }),

  'GET /user/me': {
    controller: 'UserController',
    action: 'me'
  },

  'GET /user/:id': {
    controller: 'UserController',
    action: 'tasterDashboard'
  },

  'GET /users': {
    controller: 'UserController',
    action: 'users'
  },

  'GET /user/count': {
    controller: 'UserController',
    action: 'countUsers'
  },

  'POST /user/add': {
    controller: 'UserController',
    action: 'addUser'
  },

  'POST /user/edit/:id': {
    controller: 'UserController',
    action: 'editUser'
  },

  'PATCH /user/:id': {
    controller: 'UserController',
    action: 'updateUser'
  },

  'POST /update-user-password': {
    controller: 'UserController',
    action: 'updateUserPassword'
  },

  'GET /organizations': {
    controller: 'OrganizationController',
    action: 'organizations'
  },

  'GET /organizationsNumber': {
    controller: 'OrganizationController',
    action: 'countOrganizations'
  },

  'POST /organization/add': {
    controller: 'OrganizationController',
    action: 'addOrganization'
  },

  'POST /organization/edit/:id': {
    controller: 'OrganizationController',
    action: 'editOrganization'
  },

  'DELETE /organization/:id': {
    controller: 'OrganizationController',
    action: 'deleteOrganization'
  },

  'GET /organization': {
    controller: 'OrganizationController',
    action: 'organization'
  },

  'DELETE /user/:id': {
    controller: 'UserController',
    action: 'deleteUser'
  },

  'POST /email': {
    controller: 'SurveyController',
    action: 'dispatchEmail'
  },

  'POST /user/createtasteraccount': {
    controller: 'UserController',
    action: 'createTasterAccount'
  },

  'POST /user/tasterResendVerificationEmail': {
    controller: 'UserController',
    action: 'tasterResendVerificationEmail'
  },

  'POST /user/updatetasteraccount': {
    controller: 'UserController',
    action: 'updateTasterAccount'
  },

  'POST /user/requestaccount': {
    controller: 'UserController',
    action: 'dispatchRequestAccountEmail'
  },

  'GET /survey/getsurveysbyowner': {
    controller: 'SurveyController',
    action: 'getSurveysByOwner'
  },

  'POST /survey/getCoversOfSurveys': {
    controller: 'SurveyController',
    action: 'getCoversOfSurveys'
  },

  'POST /survey/isDuplicate': {
    controller: 'SurveyController',
    action: 'isUniqueNameDuplicated'
  },

  'POST /survey/share/stats': {
    controller: 'SurveyController',
    action: 'shareSurveyStats'
  },

  'GET /survey/count': {
    controller: 'SurveyController',
    action: 'countSurveys'
  },
  'POST /survey-pdf/operator/:surveyId/:jobGroupId': {
    controller: 'SurveyController',
    action: 'getSurveyPdf'
  },
  'POST /survey-pdf/taster/:surveyId/:jobGroupId': {
    controller: 'SurveyController',
    action: 'getTasterPdf'
  },

  'POST /survey': {
    controller: 'SurveyController',
    action: 'createSurvey'
  },

  'GET /pdf/:id': {
    controller: 'SurveyController',
    action: 'downloadPdf'
  },

  'PATCH /survey/:id': {
    controller: 'SurveyController',
    action: 'updateSurvey'
  },

  'POST /system/resetShareLinks': {
    controller: 'SystemController',
    action: 'resetShareLinks'
  },

  'POST /user/logintosurvey': {
    controller: 'UserController',
    action: 'loginToSurvey'
  },

  'POST /user/logintosurveywithauth': {
    controller: 'UserController',
    action: 'loginToSurveyWithAuth'
  },

  'POST /user/forgotpassword': {
    controller: 'UserController',
    action: 'forgotPassword'
  },

  'POST /user/isregistered': {
    controller: 'UserController',
    action: 'isRegistered'
  },

  'POST /user/resetpassword': {
    controller: 'UserController',
    action: 'resetPassword'
  },
  'POST /user/verifyTaster': {
    controller: 'UserController',
    action: 'verifyTaster'
  },

  'POST /getsignedurl': uploadService.getSignedUrl,

  'GET /qrCode/getTargetLink': {
    controller: 'QrCodeController',
    action: 'getTargetLink'
  },

  'GET /qrCode/:id/download': {
    controller: 'QrCodeController',
    action: 'download'
  },

  'POST /surveyenrollment/submitanswer': {
    controller: 'SurveyEnrollmentController',
    action: 'submitAnswer'
  },

  'POST /surveyenrollement/signuptosurvey': {
    controller: 'SurveyEnrollmentController',
    action: 'submitForcedSignUp'
  },

  'POST /surveyenrollment/saveRewards': {
    controller: 'SurveyEnrollmentController',
    action: 'saveRewards'
  },

  'POST /survey/clone/:id': {
    controller: 'SurveyController',
    action: 'cloneSurvey'
  },

  'POST /survey/deprecate/:id': {
    controller: 'SurveyController',
    action: 'deprecateSurvey'
  },

  'POST /surveyenrollment/selectedproducts': {
    controller: 'SurveyEnrollmentController',
    action: 'setSurveySelectedProducts'
  },

  'POST /surveyenrollment/paypalemail': {
    controller: 'SurveyEnrollmentController',
    action: 'setPaypalEmail'
  },

  'POST /surveyenrollment/state': {
    controller: 'SurveyEnrollmentController',
    action: 'setState'
  },

  'POST /surveyenrollment/finishsurvey': {
    controller: 'SurveyEnrollmentController',
    action: 'finishSurvey'
  },

  'POST /surveyenrollment/rejectSurvey': {
    controller: 'SurveyEnrollmentController',
    action: 'rejectSurvey'
  },

  'POST /surveyenrollment/finishsurveyscreening': {
    controller: 'SurveyEnrollmentController',
    action: 'finishSurveyScreening'
  },

  'GET /loaderio-ac0a440dc991cf359fc392b6ccd38962': (req, res) => {
    const readStream = fs.createReadStream(
      path.resolve(
        sails.config.appPath,
        'loaderio-ac0a440dc991cf359fc392b6ccd38962.txt'
      )
    )
    readStream.pipe(res)
  },

  'GET /demosurvey/:demoName': {
    controller: 'SurveyController',
    action: 'getDemoSurvey'
  },

  'GET /survey/:identifier': {
    controller: 'SurveyController',
    action: 'getSurvey'
  },

  'GET /survey/download/shares/:id': {
    controller: 'SurveyController',
    action: 'getSurveyShareStats'
  },

  'GET /survey/download/gift-card-shares/:id': {
    controller: 'SurveyController',
    action: 'getGiftCardShares'
  },

  'GET /survey/download/product-incentives/:id': {
    controller: 'SurveyController',
    action: 'getProductIncentiveStats'
  },

  'GET /survey/download/gift-card-incentives/:id': {
    controller: 'SurveyController',
    action: 'getGiftCardIncentives'
  },

  'GET /surveybyorganization/:identifier': {
    controller: 'SurveyController',
    action: 'getSurveyByOrganization'
  },

  'POST /login': {
    controller: 'UserController',
    action: 'loginUser'
  },

  'POST /question/findids': {
    controller: 'QuestionController',
    action: 'findIds'
  },

  'DELETE /question/:questionId': {
    controller: 'QuestionController',
    action: 'delete'
  },

  'POST /question/replaceQuestions': {
    controller: 'QuestionController',
    action: 'replaceQuestions'
  },

  'POST /product/replaceProducts': {
    controller: 'ProductController',
    action: 'replaceProducts'
  },

  'POST /pairquestion/generatepairs': {
    controller: 'PairQuestion',
    action: 'generatePairs'
  },

  'POST /pairquestion/updatepairs': {
    controller: 'PairQuestion',
    action: 'updatePairs'
  },

  'GET /qrCode/count': {
    controller: 'QrCodeController',
    action: 'count'
  },

  'GET /surveyenrollement/validation': {
    controller: 'SurveyEnrollmentController',
    action: 'getSurveyEnrollementValidation'
  },

  'GET /surveyenrollement/funnel': {
    controller: 'SurveyEnrollmentController',
    action: 'getSurveyFunnel'
  },

  'POST /surveyenrollement/sendSurveyStats': {
    controller: 'SurveyEnrollmentController',
    action: 'sendSurveyStats'
  },

  'GET /survey/:surveyId/screeners': {
    controller: 'SurveyController',
    action: 'getScreenerSurveys'
  },

  'GET /answers/photos': {
    controller: 'AnswerController',
    action: 'getSurveyPhotos'
  },
  'GET /surveyAnswers/:enrollmentId': {
    controller: 'AnswerController',
    action: 'getSurveyAnswers'
  },
  'POST /surveyenrollement/validation/:id': {
    controller: 'SurveyEnrollmentController',
    action: 'updateSurveyEnrollementValidation'
  },

  'POST /surveyenrollement/product-incentive-payment/:id': {
    controller: 'SurveyEnrollmentController',
    action: 'updateSurveyEnrollementProductIncentives'
  },

  'POST /surveyenrollement/gift-card-incentive-payment/:id': {
    controller: 'SurveyEnrollmentController',
    action: 'updateSurveyEnrollementGiftCardIncentives'
  },

  'POST /surveyenrollement/survey-shares-payment/:id': {
    controller: 'SurveyEnrollmentController',
    action: 'updateSurveyShareStatus'
  },

  'GET /qrCodeByOrganization/:identifier': {
    controller: 'QrCodeController',
    action: 'qrCodeByOrganization'
  },

  'POST /image/add': {
    controller: 'ImageController',
    action: 'addImage'
  },

  'GET /settings/charts/:surveyId': {
    controller: 'SettingsController',
    action: 'getChartsSettings'
  },

  'POST /settings/charts': {
    controller: 'SettingsController',
    action: 'saveChartsSettings'
  }
}
