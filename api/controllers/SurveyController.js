/* globals Survey, Settings, SurveyEnrollment, Answer, Product, User, Question, emailService, ProductDisplay */
/**
 * SurveyController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
// eslint-disable-next-line node/no-extraneous-require
const ObjectId = require('mongodb').ObjectID
const s3Utils = require('../../utils/s3Utils')
const R = require('ramda')
const defaultCurrencies = require('../../utils/defaultCurrencies')
const requestUtils = require('../../utils/requestUtils')
const { getScreenerSurveys } = require('../services/surveyService')
const {
  surveySearch,
  createSurveySearchPipeline
} = require('../services/searches')
const currencyNotations = require('../../utils/currencyNotations')
const { createSurveyShareLink } = require('../services/shareService')
const puppeteer = require('puppeteer')
const pdfCreation = require('../../utils/pdfCreation')
const {
  createDisplayCombination,
  updateDisplayCombination
} = require('../../utils/ProductDisplayCombination')
const fs = require('fs')
const shortid = require('shortid')

const SURVEY_EMAIL_TYPES = [
  'survey-completed',
  'survey-waiting',
  'survey-rejected'
]

const printResultSet = async (
  surveyId,
  jobGroupId,
  layout,
  templateName,
  footer
) => {
  const browser = await puppeteer.launch({
    headless: true,
    ignoreHTTPSErrors: true,
    args: [
      '--no-sandbox',
      '--proxy-server=https=xxx:xxx',
      '--disable-dev-shm-usage'
    ]
  })
  const page = await browser.newPage()
  await page.setRequestInterception(true)
  page.on('request', req => {
    const url = req.url()
    // optimize: block unnecessary
    const BLOCK_URLS = [
      '://cdn.slaask.com',
      '://slaask.com',
      '://uploads.slaask.com',
      '://ws.pusherapp.com',
      '://stats.pusher.com'
    ]
    if (BLOCK_URLS.some(BLOCK_URL => url.includes(BLOCK_URL))) {
      req.abort()
    } else {
      req.continue()
    }
  })
  await page.setViewport({
    width: 1920,
    height: 900
  })

  await page.goto(process.env.APP_URL, { waitUntil: 'domcontentloaded' })

  await page.evaluate(
    (layout, footer) => {
      /* eslint-disable */
      if (layout) {
        localStorage.setItem('pdf-layout', JSON.stringify(layout))
      }
      localStorage.setItem('footer-note', footer || '')
      /* eslint-enable */
    },
    layout,
    footer
  )

  await page.goto(
    `${
      process.env.APP_URL
    }/preview-${templateName}-pdf/${surveyId}/${jobGroupId}`,
    {
      waitUntil: 'networkidle0'
    }
  )
  await page.waitForSelector('#loading', { hidden: true })

  const downloadFile = `${surveyId}-${shortid.generate()}`
  const pathForPdf = await pdfCreation(downloadFile)
  const { docHeight, pageHeight } = await page.evaluate(() => ({
    docHeight: document.documentElement.scrollHeight,
    pageHeight: window.__PAGE_HEIGHT
  }))
  await page.pdf({
    path: pathForPdf,
    format: (layout && layout.paper) || 'A4',
    printBackground: true,
    displayHeaderFooter: false,
    pageRanges: `1-${Math.ceil(docHeight / pageHeight)}`,
    margin: {
      top: 0,
      bottom: 0,
      left: 0,
      right: 0
    }
  })
  await browser.close()

  return downloadFile
}

const repairEmptyRewards = async ({ survey }) => {
  try {
    const emptySurveyEnrollments = await SurveyEnrollment.find({
      survey: survey.id,
      paypalEmail: { '!=': [''] },
      state: 'finished',
      productIncentiveExported: false,
      validation: 'valid',
      savedRewards: ['', []]
    }).populate('selectedProducts')

    if (!emptySurveyEnrollments.length) {
      return { hasError: false }
    }
    for (let count = 0; count < emptySurveyEnrollments.length; count++) {
      const currentSurveyEnrollment = emptySurveyEnrollments[count]
      const { selectedProducts } = currentSurveyEnrollment

      if (selectedProducts && selectedProducts.length) {
        const selectedIds = selectedProducts.map(product => product.id)
        const newSavedRewards = survey.products.map(product => {
          return {
            id: product.id,
            reward: product.reward,
            answered: selectedIds.indexOf(product.id) >= 0
          }
        })
        await SurveyEnrollment.updateOne({
          id: currentSurveyEnrollment.id
        }).set({
          savedRewards: newSavedRewards
        })
      }
    }
    return { hasError: false }
  } catch (error) {
    return { hasError: true, errorDetail: error }
  }
}

const getSurveyPdfLayout = async (surveyId, prop) => {
  const surveySetting = await Settings.findOne({
    where: { surveyId }
  })
  if (surveySetting) {
    return surveySetting[prop]
  }
  return null
}

module.exports = {
  isUniqueNameDuplicated: async (req, res) => {
    try {
      const { uniqueName, surveyId } = req.body

      const surveys = await Survey.find({ uniqueName: uniqueName })
      if (!surveys.length) {
        return res.ok(false)
      } else if (
        surveys.length === 1 &&
        surveyId &&
        surveys[0]['id'] === surveyId
      ) {
        return res.ok(false)
      }
      return res.ok(true)
    } catch (error) {
      res.badRequest(error)
    }
  },
  createSurvey: async (req, res) => {
    try {
      const {
        predecessorSurvey,
        exclusiveTasters,
        products,
        productDisplayType
      } = req.body
      const surveyToCreate = R.omit(['exclusiveTaster'], req.body)
      const { user } = req.token

      let creator = user.id
      if (predecessorSurvey) {
        const survey = await Survey.findOne({
          id: predecessorSurvey
        })
        creator = survey.creator
        surveyToCreate['emails'] = survey.emails
      }

      let tasterIds
      if (exclusiveTasters) {
        const populatedTasters = await User.find({
          where: { emailAddress: exclusiveTasters }
        })
        tasterIds = populatedTasters.map(taster => taster.id)
      }

      const createdSurvey = await Survey.create(
        Object.assign({}, surveyToCreate, {
          exclusiveTasters: tasterIds,
          creator: creator,
          lastModifier: user.id
        })
      ).fetch()

      const { id } = createdSurvey
      await createDisplayCombination(products, productDisplayType, id)

      createSurveyShareLink(createdSurvey)

      return res.ok(createdSurvey)
    } catch (error) {
      res.badRequest(error)
    }
  },
  updateSurvey: async (req, res) => {
    try {
      if (!req.token) {
        return res.unauthorized()
      }
      const { id } = req.params
      const { exclusiveTasters, products } = req.body
      const fieldsToUpdate = R.omit(['exclusiveTaster'], req.body)
      const { user } = req.token
      const survey = await Survey.findOne({ id: id })

      if (survey && survey.owner !== user.organization) {
        return res.unauthorized()
      }

      let tasterIds
      if (exclusiveTasters) {
        const populatedTasters = await User.find({
          where: { emailAddress: exclusiveTasters }
        })
        tasterIds = populatedTasters.map(taster => taster.id)
      }

      let questions = await Question.find({
        survey: id
      })

      if (fieldsToUpdate.state === 'active') {
        if (survey.state === 'draft') {
          for (let question of questions) {
            await Answer.destroy({
              question: question.id
            })
          }
        }
      }

      for (let question of questions) {
        if (
          question.typeOfQuestion === 'paypal-email' &&
          'referralAmount' in fieldsToUpdate &&
          !fieldsToUpdate.referralAmount
        ) {
          await Question.destroy({
            id: question.id
          })
        }
      }

      const updatedSurvey = await Survey.updateOne({
        id
      }).set(
        Object.assign({}, fieldsToUpdate, {
          exclusiveTasters: tasterIds,
          lastModifier: user.id
        })
      )

      createSurveyShareLink(survey)

      const { productDisplayType } = updatedSurvey
      if (productDisplayType === 'none') {
        await updateDisplayCombination(products, productDisplayType, id)
      }

      return res.ok(updatedSurvey)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  getScreenerSurveys: async (req, res) => {
    const { surveyId } = req.params
    const screenerSurveys = await getScreenerSurveys(surveyId)
    res.ok(screenerSurveys.map(survey => ({ id: survey._id, ...survey })))
  },
  getSurveysByOwner: async (req, res) => {
    try {
      const {
        token: {
          user: { organization: owner, type, id }
        }
      } = req

      const searchQuery = {
        ...requestUtils.parseSearchQuery(req),
        userType: type,
        owner,
        userId: id
      }
      const pipeline = createSurveySearchPipeline(searchQuery)
      const { skip, limit } = searchQuery

      const surveys = await surveySearch.search(pipeline, {
        skip,
        limit,
        orderBy: { createdAt: -1 }
      })

      return res.ok(surveys)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  countSurveys: async (req, res) => {
    try {
      const {
        token: {
          user: { organization: owner, type, id }
        }
      } = req

      const searchQuery = {
        ...requestUtils.parseSearchQuery(req),
        userType: type,
        owner,
        userId: id
      }
      const pipeline = createSurveySearchPipeline(searchQuery, {
        populateProducts: false
      })

      const count = await surveySearch.count(pipeline)

      return res.ok(count)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  getDemoSurvey: async (req, res) => {
    try {
      const { demoName } = req.params
      const survey = await Survey.findOne({ uniqueName: demoName })
      if (!survey) {
        return res.badRequest(`Could not find survey with the name ${demoName}`)
      }
      return res.ok(survey)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  getSurvey: async (req, res) => {
    try {
      const { identifier } = req.params
      let survey = await Survey.findOne({ uniqueName: identifier })
      if (survey) {
        return res.redirect('/survey/' + survey.id)
      }
      survey = await Survey.findOne({ id: identifier })
        // .populate('creator')
        // .populate('lastModifier')
        .populate('questions')
        .populate('products', {
          sort: 'createdAt ASC'
        })
        // .populate('exclusiveTasters')
        // .populate('sharedStatsUsers')
        .populate('linkedSurveys')

      if (survey) {
        return res.ok(survey)
      }
      return res.badRequest(
        `Could not find survey with the name or id ${identifier}`
      )
    } catch (error) {
      return res.badRequest(error)
    }
  },
  getSurveyByOrganization: async (req, res) => {
    try {
      if (!req.token) {
        return res.unauthorized()
      }
      const { identifier } = req.params
      const { user } = req.token

      let survey = await Survey.findOne({ uniqueName: identifier })
      if (survey) {
        return res.redirect('/survey/' + survey.id)
      }
      survey = await Survey.findOne({ id: identifier })
        .populate('creator')
        .populate('lastModifier')
        .populate('questions')
        .populate('exclusiveTasters')
        .populate('sharedStatsUsers')
        .populate('linkedSurveys')
        .populate('products', {
          sort: 'createdAt ASC'
        })

      if (survey && survey.owner === user.organization) {
        return res.ok(survey)
      }
      if (survey) {
        const { sharedStatsUsers } = survey
        const sharedUsers = sharedStatsUsers.length
          ? R.pluck('id', sharedStatsUsers)
          : []
        if (sharedUsers.indexOf(user.id) >= 0) {
          return res.ok(survey)
        }
      }
      return res.badRequest(
        `Could not find survey with the name or id ${identifier}`
      )
    } catch (error) {
      return res.badRequest(error)
    }
  },
  cloneSurvey: async (req, res) => {
    try {
      // Get survey id to be cloned
      const id = req.param('id')

      if (!id) {
        throw new Error('Survey Id is required for the Dispatcher')
      }

      // Fetch survey to clone
      const survey = await Survey.findOne({ id })
        .populate('products')
        .populate('questions')

      // Product display grab
      const productDisplayFromDb = await ProductDisplay.findOne({
        survey: id
      })

      const productDisplay = productDisplayFromDb
        ? R.omit(['id', 'survey'], productDisplayFromDb)
        : []

      const { products = [], questions = [], uniqueName } = survey

      // Remove old data which are not needed to clone
      const clonedSurveyData = R.omit(
        ['products', 'questions', 'id', 'createdAt', 'updatedAt'],
        survey
      )

      const clonedProducts = await Promise.all(
        products.map(async product => {
          let { photo } = product
          let photoKey = s3Utils.getObjectKey(photo)
          const mainProductId = product && product.id
          const clonedProduct = R.omit(['id', 'photo'], product)

          if (photoKey) {
            let timestamp = new Date().valueOf()
            let newPhotoKey = `copy-at-${timestamp}-of-${photoKey}`
            photo = await s3Utils.copyFile(photoKey, newPhotoKey)
          }

          const clonedProductDoc = await Product.create({
            ...clonedProduct,
            photo
          }).fetch()
          const clonedProductId = clonedProductDoc && clonedProductDoc.id
          if (productDisplay && productDisplay.products) {
            const indexOfMainProductId = productDisplay.products.indexOf(
              mainProductId
            )
            if (indexOfMainProductId > -1) {
              productDisplay.products[indexOfMainProductId] = clonedProductId
            }
          }
          return clonedProductDoc
        })
      )

      // Create list of product ids to associate to new survey
      const mapId = R.map(R.prop('id'))

      const clonedSurvey = await Survey.create(
        Object.assign({}, clonedSurveyData, {
          products: mapId(clonedProducts),
          name: `copy of ${clonedSurveyData.name}`,
          state: 'draft',
          uniqueName: `copy-of-${uniqueName}-${new Date().valueOf()}`
        })
      ).fetch()

      // Add new entry in product Display
      if (productDisplay && productDisplay.products && clonedSurvey) {
        productDisplay['survey'] = clonedSurvey.id
        await ProductDisplay.create(productDisplay)
      }

      // Clone questions
      // is store newQuestionId by questionId
      const newIds = {}
      const getNewId = id => newIds[id]

      for (const question of questions) {
        // Remove old attributes
        const clonedQuestionData = R.omit(
          ['id', 'createdAt', 'updatedAt', 'survey', 'skipFlow'],
          question
        )

        // Create questions with new survey associated
        const clonedQuestion = await Question.create(
          Object.assign({}, clonedQuestionData, { survey: clonedSurvey.id })
        ).fetch()

        newIds[question.id] = clonedQuestion.id
      }

      // reindex nextQuestion
      for (let question of questions) {
        const { id, nextQuestion, relatedQuestions } = question
        await Question.updateOne({ id: getNewId(id) }).set({
          nextQuestion: getNewId(nextQuestion),
          relatedQuestions: relatedQuestions
            ? R.map(getNewId)(relatedQuestions)
            : null
        })
      }

      return res.ok({ survey: clonedSurvey.id })
    } catch (error) {
      return res.badRequest(error)
    }
  },
  deprecateSurvey: async (req, res) => {
    try {
      if (!req.token) {
        return res.unauthorized()
      }
      const { user } = req.token
      const id = req.param('id')

      if (!id) {
        throw new Error('Survey Id is required for the Dispatcher')
      }

      const survey = await Survey.findOne({ id })

      if (survey && survey.owner !== user.organization) {
        return res.ok(survey)
      }

      const { uniqueName } = survey

      await Survey.updateOne({ id }).set({
        state: 'deprecated',
        uniqueName: `${uniqueName}-deprecated-at-${new Date().toISOString()}`
      })
      return res.ok(id)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  getSurveyShareStats: async (req, res) => {
    try {
      if (!req.token) {
        return res.unauthorized()
      }
      const { user } = req.token
      const id = req.param('id')

      if (!id) {
        throw new Error('Survey Id is required for the Dispatcher')
      }

      const survey = await Survey.findOne({ id }).populate('linkedSurveys')

      if (survey && survey.owner !== user.organization) {
        return res.unauthorized()
      }

      let allSurveyEnrollments = []

      if (survey.isScreenerOnly) {
        const linkedSurveyIds = R.pluck('id', survey.linkedSurveys)
        const screenerSurveyEnrollments = await SurveyEnrollment.find({
          where: {
            survey: id,
            referral: { '!=': [''] },
            state: 'finished',
            shareExported: false
          },
          select: ['id', 'user', 'referral']
        }).populate('user')
        const userIds = []
        screenerSurveyEnrollments.forEach(screenerEnrollment => {
          if (
            screenerEnrollment.user &&
            !screenerEnrollment.user.completedReferredSurvey
          ) {
            userIds.push(screenerEnrollment.user.id)
          }
        })
        // const users = R.pluck('id', R.pluck('user', screenerSurveyEnrollments))
        if (userIds.length) {
          const linkedSurveyEnrollments = await SurveyEnrollment.find({
            where: {
              survey: { in: linkedSurveyIds },
              user: { in: userIds },
              state: 'finished',
              validation: 'valid',
              shareExported: false
            },
            select: ['id', 'user']
          })

          linkedSurveyEnrollments.forEach(linkedEnrollment => {
            screenerSurveyEnrollments.forEach(screenerEnrollment => {
              if (
                screenerEnrollment.user &&
                screenerEnrollment.user.id === linkedEnrollment.user
              ) {
                allSurveyEnrollments.push(screenerEnrollment)
              }
            })
          })
        }
      } else {
        allSurveyEnrollments = await SurveyEnrollment.find({
          where: {
            survey: id,
            referral: { '!=': [''] },
            state: 'finished',
            validation: 'valid',
            shareExported: false
          },
          select: ['id', 'referral']
        })
      }

      if (allSurveyEnrollments.length) {
        const allReferrals = R.pluck('referral', allSurveyEnrollments)
        const frequencyCount = R.countBy(R.identity, allReferrals)
        const allRewardEnrollements = await SurveyEnrollment.find({
          where: {
            id: { in: R.uniq(allReferrals) },
            paypalEmail: { '!=': [''] }
          },
          select: ['paypalEmail']
        })

        if (allRewardEnrollements.length) {
          const resultant = []
          for (let count = 0; count < allRewardEnrollements.length; count++) {
            let refferedIds = []
            const referralCount =
              frequencyCount[allRewardEnrollements[count].id]
            const email = allRewardEnrollements[count].paypalEmail
            const amount = referralCount
              ? referralCount * survey.referralAmount
              : survey.referralAmount
            const currency =
              survey.referralRewardCurrency ||
              defaultCurrencies[survey.country] ||
              'USD'

            allSurveyEnrollments.forEach(surveyEnrollment => {
              if (
                surveyEnrollment.referral === allRewardEnrollements[count].id
              ) {
                refferedIds.push(surveyEnrollment.id)
              }
            })
            resultant.push({
              email,
              amount,
              currency,
              refferedIds
            })
          }
          return res.ok(resultant)
        }
      }
      return res.ok([])
    } catch (error) {
      return res.badRequest(error)
    }
  },
  getGiftCardShares: async (req, res) => {
    try {
      if (!req.token) {
        return res.unauthorized()
      }
      const { user } = req.token
      const id = req.param('id')

      if (!id) {
        throw new Error('Survey Id is required for the Dispatcher')
      }

      const survey = await Survey.findOne({ id })

      if (survey && survey.owner !== user.organization) {
        return res.unauthorized()
      }

      const allSurveyEnrollments = await SurveyEnrollment.find({
        where: {
          survey: id,
          referral: { '!=': [''] },
          state: 'finished',
          validation: 'valid',
          shareExported: false
        },
        select: ['id', 'referral']
      })
      if (allSurveyEnrollments.length) {
        const allReferrals = R.pluck('referral', allSurveyEnrollments)
        const frequencyCount = R.countBy(R.identity, allReferrals)
        const allRewardEnrollements = await SurveyEnrollment.find({
          where: {
            id: { in: R.uniq(allReferrals) },
            user: { '!=': [''] },
            paypalEmail: ''
          },
          select: ['id', 'user']
        }).populate('user')

        if (allRewardEnrollements.length) {
          const resultant = []
          for (let count = 0; count < allRewardEnrollements.length; count++) {
            let refferedIds = []
            const referralCount =
              frequencyCount[allRewardEnrollements[count].id]
            const email = allRewardEnrollements[count].user.emailAddress
            const amount = referralCount
              ? referralCount * survey.referralAmount
              : survey.referralAmount
            const currency =
              survey.referralRewardCurrency ||
              defaultCurrencies[survey.country] ||
              'USD'

            allSurveyEnrollments.forEach(surveyEnrollment => {
              if (
                surveyEnrollment.referral === allRewardEnrollements[count].id
              ) {
                refferedIds.push(surveyEnrollment.id)
              }
            })
            resultant.push({
              email,
              amount,
              currency,
              refferedIds
            })
          }
          return res.ok(resultant)
        }
      }
      return res.ok([])
    } catch (error) {
      return res.badRequest(error)
    }
  },
  getProductIncentiveStats: async (req, res) => {
    try {
      if (!req.token) {
        return res.unauthorized()
      }
      const { user } = req.token
      const id = req.param('id')

      if (!id) {
        throw new Error('Survey Id is required for the Dispatcher')
      }

      const survey = await Survey.findOne({ id }).populate('products')

      if (survey && survey.owner !== user.organization) {
        return res.unauthorized()
      }

      if (survey && !survey.isPaypalSelected) {
        return res.badRequest('Paypal is not available for this survey')
      }

      const { hasError, errorDetail } = await repairEmptyRewards({ survey })
      if (hasError) {
        return res.badRequest(errorDetail)
      }

      const allSurveyEnrollments = await SurveyEnrollment.find({
        where: {
          survey: id,
          paypalEmail: { '!=': [''] },
          savedRewards: { '!=': ['', []] },
          state: 'finished',
          validation: 'valid',
          productIncentiveExported: false
        },
        select: ['id', 'paypalEmail', 'savedRewards', 'referral']
      })
      if (allSurveyEnrollments.length) {
        const resultant = []
        for (
          let counter = 0;
          counter < allSurveyEnrollments.length;
          counter++
        ) {
          const currentEnrollement = allSurveyEnrollments[counter]
          const totalReward = R.sum(
            R.pluck(
              'reward',
              R.filter(
                reward => reward.answered,
                currentEnrollement.savedRewards
              )
            )
          )
          resultant.push({
            id: currentEnrollement['id'],
            email: currentEnrollement['paypalEmail'],
            amount: totalReward,
            referral: currentEnrollement['referral'],
            currency:
              survey.referralRewardCurrency ||
              defaultCurrencies[survey.country] ||
              'USD'
          })
        }
        return res.ok(resultant)
      }
      return res.ok([])
    } catch (error) {
      return res.badRequest(error)
    }
  },

  getGiftCardIncentives: async (req, res) => {
    try {
      if (!req.token) {
        return res.unauthorized()
      }
      const { user } = req.token
      const id = req.param('id')

      if (!id) {
        throw new Error('Survey Id is required for the Dispatcher')
      }

      const survey = await Survey.findOne({ id }).populate('products')

      if (survey && survey.owner !== user.organization) {
        return res.unauthorized()
      }

      if (
        survey &&
        (!survey.isGiftCardSelected || survey.authorizationType !== 'email')
      ) {
        return res.badRequest('Gift Card is not available for this survey')
      }

      const { hasError, errorDetail } = await repairEmptyRewards({ survey })
      if (hasError) {
        return res.badRequest(errorDetail)
      }

      const allSurveyEnrollments = await SurveyEnrollment.find({
        where: {
          survey: id,
          paypalEmail: '',
          state: 'finished',
          validation: 'valid',
          giftCardIncentiveExported: false
        },
        select: ['id', 'paypalEmail', 'savedRewards', 'referral', 'user']
      }).populate('user')
      if (allSurveyEnrollments.length) {
        const resultant = []
        for (
          let counter = 0;
          counter < allSurveyEnrollments.length;
          counter++
        ) {
          const currentEnrollement = allSurveyEnrollments[counter]
          const totalReward = R.sum(
            R.pluck(
              'reward',
              R.filter(
                reward => reward.answered,
                currentEnrollement.savedRewards
              )
            )
          )

          resultant.push({
            id: currentEnrollement['id'],
            email: currentEnrollement['user'].emailAddress,
            referral: currentEnrollement['referral'],
            amount: totalReward,
            currency:
              survey.referralRewardCurrency ||
              defaultCurrencies[survey.country] ||
              'USD'
          })
        }
        return res.ok(resultant)
      }
      return res.ok([])
    } catch (error) {
      return res.badRequest(error)
    }
  },
  dispatchEmail: async (req, res) => {
    try {
      const { id, email, type, surveyEnrollment } = req.body
      if (!id) {
        throw new Error('Survey Id is required for the Dispatcher')
      }
      const survey = await Survey.findOne({ id }).populate('products')
      if (!email) {
        throw new Error(`E-mail is required for the Dispatcher`)
      }
      if (!survey) {
        throw new Error(`Couldn't find the survey with the following id: ${id}`)
      }

      // Removed Because we don't need settings anymore
      // if (!survey.settings.emailService) {
      //   return res.ok()
      // }
      // if (
      //   SURVEY_EMAIL_TYPES.includes(type) &&
      //   !survey.enabledEmailTypes.includes(type)
      // ) {
      //   return res.ok()
      // }

      let shareSurey = survey
      const screenerSurveys = await getScreenerSurveys(survey.id)
      if (screenerSurveys.length > 0) {
        shareSurey = screenerSurveys[0]
      }

      const hostname = process.env.APP_URL

      const shareLink = `${hostname}/s/survey/${shareSurey.uniqueName}${
        surveyEnrollment ? `?referral=${surveyEnrollment}` : ''
      }`

      const currencyDivider =
        survey.referralRewardCurrency ||
        defaultCurrencies[survey.country] ||
        'USD'
      const currency = currencyDivider
        ? currencyNotations[currencyDivider]
        : '$'
      const amount =
        currency === '$'
          ? currency + survey.referralAmount
          : survey.referralAmount + currency
      switch (type) {
        case 'survey-completed': {
          if (!survey.isScreenerOnly) {
            await emailService.sendSurveyCompletionMail({
              email,
              shareLink,
              language: survey.surveyLanguage,
              amount,
              allowedDaysToFillTheTasting: survey.allowedDaysToFillTheTasting || 5,
              surveyEmail: survey.emails['surveyCompleted']
            })
          }

          break
        }
        case 'survey-waiting': {
          const { products } = survey
          const productsNames = products.map(product => product.name)
          const productsImages = products.map(product => product.photo)
          const url = `${hostname}/survey/${id}`
          await emailService.sendSurveyWaitingMail({
            email,
            productsNames,
            productsImages,
            url,
            language: survey.surveyLanguage,
            shareLink,
            amount,
            allowedDaysToFillTheTasting:
              survey.allowedDaysToFillTheTasting || 5,
            surveyEmail: R.path(['emails', 'surveyWaiting'])(survey)
          })
          break
        }
        default:
          throw new Error('Unknown type of e-mail service')
      }
      return res.ok()
    } catch (error) {
      return res.badRequest(error)
    }
  },
  shareSurveyStats: async (req, res) => {
    try {
      if (!req.token) {
        return res.unauthorized()
      }
      const { user } = req.token
      const { users, surveyId } = req.body

      if (!surveyId) {
        throw new Error('Survey Id is required for the Dispatcher')
      }

      const survey = await Survey.findOne({ id: surveyId })

      if (survey && survey.owner !== user.organization) {
        return res.unauthorized()
      }
      await Survey.update({ id: surveyId }).set({
        sharedStatsUsers: users
      })

      const updatedSurvey = await Survey.findOne({ id: surveyId }).populate(
        'sharedStatsUsers'
      )
      return res.ok(updatedSurvey)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  getCoversOfSurveys: async (req, res) => {
    const { surveys } = req.body

    const db = Survey.getDatastore().manager
    const cursor = await db
      .collection('product_surveys__survey_products')
      .aggregate([
        { $match: { survey_products: { $in: surveys.map(ObjectId) } } },
        {
          $lookup: {
            from: 'product',
            let: { surveys: '$product_surveys' },
            pipeline: [
              {
                $match: {
                  $expr: { $eq: ['$_id', '$$surveys'] },
                  isSurveyCover: true
                }
              }
            ],
            as: 'products'
          }
        },
        {
          $project: {
            _id: false,
            survey: '$survey_products',
            coverPhoto: { $arrayElemAt: ['$products.photo', 0] }
          }
        },
        {
          $unwind: '$coverPhoto'
        }
      ])

    res.send(await cursor.toArray())
  },
  downloadPdf: async (req, res) => {
    const file = req.params.id
    res.set('Content-Disposition', `attachment; filename=${file}.pdf`)
    fs.createReadStream(`./assets/pdf/${file}.pdf`).pipe(res)
  },
  getSurveyPdf: async (req, res) => {
    try {
      const { surveyId, jobGroupId } = req.params
      const { layout: requestLayout } = req.body

      const layout =
        requestLayout ||
        (await getSurveyPdfLayout(surveyId, 'operatorPdfLayout'))

      const survey = await Survey.findOne({
        where: { id: surveyId }
      })

      let footer = null

      if (
        survey &&
        survey.pdfFooterSettings &&
        survey.pdfFooterSettings.active
      ) {
        const responders = await SurveyEnrollment.count({
          survey: surveyId,
          state: 'finished'
        })
        const footerNote = survey.pdfFooterSettings.footerNote
        footer = `<div id="footer-template" style="z-index:1000;font-size:15px !important;
          color:#001233;
          margin-left:30px;
          -webkit-print-color-adjust:exact;
          padding-left:10px">
          <div>Note:${footerNote}.These show the subjective response obtained from <span style="
          color:#F0485E; -webkit-print-color-adjust:exact;">${responders}</span> taster(s)</div>
          </div>
        `
      }

      const downloadFile = await printResultSet(
        surveyId,
        jobGroupId,
        layout,
        'operator',
        footer
      )

      return res.ok({ downloadFile })
    } catch (error) {
      return res.badRequest(error)
    }
  },
  getTasterPdf: async (req, res) => {
    try {
      const { surveyId, jobGroupId } = req.params
      const { layout: requestLayout } = req.body

      const layout =
        requestLayout || (await getSurveyPdfLayout(surveyId, 'tasterPdfLayout'))

      const downloadFile = await printResultSet(
        surveyId,
        jobGroupId,
        layout,
        'taster'
      )

      return res.ok({ downloadFile })
    } catch (error) {
      return res.badRequest(error)
    }
  }
}
