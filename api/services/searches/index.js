module.exports = {
  ...require('./surveySearch'),
  ...require('./organizationSearch'),
  ...require('./userSearch'),
  ...require('./qrCodeSearch'),
  ...require('./answerSearch')
}
