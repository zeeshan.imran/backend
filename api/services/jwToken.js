/* global process, sails */
/**
 * jwToken
 *
 * @description :: JSON Webtoken Service for sails
 * @help        :: See https://github.com/auth0/node-jsonwebtoken & http://sailsjs.org/#!/documentation/concepts/Services
 */

const jwt = require('jsonwebtoken')

const tokenSecret = process.env.TOKEN_SECRET || sails.config.jwt.token_secret

// Generates a token from supplied payload
module.exports.issue = payload => {
  return jwt.sign(
    payload,
    tokenSecret, // Token Secret that we sign it with
    {
      expiresIn: '100 years' // Token Expire time
    }
  )
}

// Verifies token on a request
module.exports.verify = (token, callback) => {
  return jwt.verify(
    token, // The token to be verified
    tokenSecret, // Same token we used to sign
    {}, // No Option, for more see https://github.com/auth0/node-jsonwebtoken#jwtverifytoken-secretorpublickey-options-callback
    callback // Pass errors or decoded token to callback
  )
}

module.exports.decode = token => {
  return jwt.decode(token)
}
