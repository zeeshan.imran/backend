// eslint-disable-next-line node/no-extraneous-require
const { createId } = require('../utils')
const organizations = require('./organizations')

const users = [
  {
    id: createId(),
    organization: organizations[0].id,
    emailAddress: 'operator@flavorwiki.com',
    type: 'operator',
    password: 'operator'
  },
  {
    id: createId(),
    organization: organizations[1].id,
    emailAddress: 'operator@flavorwiki-2.com',
    type: 'operator',
    password: 'operator'
  },
  {
    id: createId(),
    organization: organizations[0].id,
    emailAddress: 'user@flavorwiki.com',
    type: 'user',
    password: 'user'
  },
  {
    id: createId(),
    organization: organizations[0].id,
    emailAddress: 'operator2@flavorwiki.com',
    type: 'operator',
    password: 'operator'
  },
  {
    id: createId(),
    organization: organizations[1].id,
    emailAddress: 'admin@flavorwiki.com',
    isSuperAdmin: true,
    type: 'operator',
    password: 'operator'
  }
]

module.exports = users
