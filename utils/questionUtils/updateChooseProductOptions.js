/* globals Survey */
module.exports = async question => {
  if (question.chooseProductOptions) {
    const { minimumProducts, maximumProducts } = question.chooseProductOptions
    await Survey.updateOne({ id: question.survey }).set({
      minimumProducts,
      maximumProducts
    })
  }
}
