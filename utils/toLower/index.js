const toLower = str => (!str ? str : str.toString().toLowerCase())
module.exports = toLower
