/* globals sails */
/* eslint-disable */
const Combinatorics = require('js-combinatorics')
/* eslint-enable */

const createSet = async (model, elements) => {
  const createdElements = await model.createEach(elements).fetch()

  return createdElements
}

const generatePairs = values =>
  Combinatorics.combination(values, 2)
    .toArray()
    .reduce(
      (permutations, combination) => [
        ...permutations,
        {
          leftAttribute: combination[0],
          rightAttribute: combination[1],
          leftAttributeCounter: 0,
          rightAttributeCounter: 0
        },
        {
          leftAttribute: combination[1],
          rightAttribute: combination[0],
          leftAttributeCounter: 0,
          rightAttributeCounter: 0
        }
      ],
      []
    )

const generateQuestionSection = (questions, section) =>
  questions.map((question, index) => {
    question.displayOn = section
    question.requiredQuestion =
      question.requiredQuestion !== undefined ? question.requiredQuestion : true
    question.order = index
    return question
  })

const generateProfileRelatedQuestions = async (
  pairedQuestions,
  profileQuestions
) => {
  const pairedQuestionsId = pairedQuestions.map(pairQuestion => pairQuestion.id)
  const profileQuestionsId = profileQuestions.map(
    profileQuestion => profileQuestion.id
  )

  await Question.update({
    id: profileQuestionsId
  }).set({
    relatedQuestions: pairedQuestionsId
  })
}

module.exports = {
  createSet,
  generatePairs,
  generateQuestionSection,
  generateProfileRelatedQuestions
}
